#!/bin/sh

MAKE=make_dgac.rsp
OLD="MONO_HOME"
NEW=$MONO_HOME

cp $MAKE temp.rsp
find ./ -iname temp.rsp | xargs sed -i --expression "s@$OLD@$NEW@"

if [ ! -d ./bin ]
then
        mkdir bin
fi

if [ ! -f DGAC/DGAC.snk ]
then
    sn -k DGAC/DGAC.snk
fi

mcs @temp.rsp
rm temp.rsp

./install_DGAC.sh
#/usr/local/bin/mpicc -O -fpic -shared -o $MONO_HOME/lib/mono/lib_c/libhpcshelf.so libhpcshelf.c
#/usr/local/bin/mpic++ -O -fpic -shared -o $MONO_HOME/lib/mono/lib_c/libhpcshelf++.so libhpcshelf.c
/usr/local/bin/mpicc -c -O -o libhpcshelf.o libhpcshelf.c
ar rcs $MONO_HOME/lib/mono/lib_c/libhpcshelf.a libhpcshelf.o
/usr/local/bin/mpic++ -c -O -o libhpcshelf++.o libhpcshelf.c
ar rcs $MONO_HOME/lib/mono/lib_c/libhpcshelf++.a libhpcshelf++.o

