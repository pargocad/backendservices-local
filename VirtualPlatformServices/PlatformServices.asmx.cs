﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Services;
using System.Web.Services.Protocols;
using DGAC;
using org.hpcshelf.DGAC;
using org.hpcshelf.DGAC.utils;
using org.hpcshelf.exception;

namespace org.hpcshelf.platform
{

    public class PlatformServices : WebService, IPlatformServices
    {
        [WebMethod]
        public int getNumberOfNodes()
        {
            string path_hosts_file = System.Environment.GetEnvironmentVariable("PATH_HOSTS_FILE");
            if (path_hosts_file == null || path_hosts_file.Equals(""))
                path_hosts_file = System.Environment.GetEnvironmentVariable("HOME") + Path.DirectorySeparatorChar + "hpe_nodes";

            string[] contents = File.ReadAllLines(path_hosts_file);
            int size = contents.Length;

            Console.WriteLine("getNumberOfNodes was called and returned " + size);

            return size - 1;
        }


        [WebMethod]
        public int getBaseBindingPort()
        {
            Console.WriteLine("THE PATH: {0}",Environment.GetEnvironmentVariable("PATH"));
            Console.WriteLine("The LD_LIBRARY_PATH: {0}", Environment.GetEnvironmentVariable("LD_LIBRARY_PATH"));

            int base_binding_port = -1;

            try
            {
                base_binding_port = Constants.BASE_BINDING_FACET_PORT;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.Message);
                Console.Error.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.Error.WriteLine(e.InnerException.Message);
                    Console.Error.WriteLine(e.InnerException.StackTrace);
                }
            }
            return base_binding_port;
        }

        private IDictionary<string, IDeployer> deployer = new Dictionary<string, IDeployer>() { 
                                                                                                { "C# Wrapper", new CSharWrapperDeployer() }, 
                                                                                                { "C Interoperability", new CInteroperabilityDeployer() },
																								{ "C/MPI Interoperability", new CMPIInteroperabilityDeployer() },
                                                                                                { "C++ Interoperability", new CPlusPlusInteroperabilityDeployer() },
                                                                                                { "Python/MPI Interoperability", new PythonMPIInteroperabilityDeployer() },
                                                                                                { "Julia/MPI Interoperability", new JuliaMPIInteroperabilityDeployer() }
                                                                                              };

		[WebMethod]
		public string deploy(string module_data, byte[] key_file_contents)
		{
            try
            {
                DeployArguments.DeployArgumentsUnitType infoCompile = LoaderApp.deserialize<DeployArguments.DeployArgumentsUnitType>(module_data);

                // infoCompile.deploy_type;

                //string path = Path.Combine(Constants.PATH_GAC, infoCompile.library_path);

                //string tempFileName = Path.Combine(Constants.PATH_TEMP_WORKER, infoCompile.moduleName + ".backend.dll");

                if (/*!File.Exists(tempFileName)*/ true)
                {
                    CSharWrapperDeployer manager = new CSharWrapperDeployer();
                    Console.WriteLine("DEPLOY TYPE = {0}, NAME={1} -- {2}", infoCompile.deploy_type, infoCompile.moduleName, deployer.ContainsKey(infoCompile.deploy_type));
                    string publicKey = deployer[infoCompile.deploy_type].deploy(infoCompile, key_file_contents);
                    return publicKey;
                }
                else
                {
                    Console.WriteLine("{0} already deployed !", infoCompile.library_path);
                    return "already deployed !";
                }
            }
            catch (HPCShelfException e)
            {
                throw WebServicesException.raiseSoapException(e, "VirtualPlatformServices.deploy");
            }
            catch (SoapException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message); Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message); Console.WriteLine(e.InnerException.StackTrace);
                }

                throw WebServicesException.raiseSoapException("HPCShelfWebServices",
                                                              "http://tempuri.org/HPCShelfWebServices",
                                                              e.Message,
                                                              ErrorCodes.GENERAL_ERROR.ToString(),
                                                              "VirtualPlatformServices.deploy", WebServicesException.FaultCode.Server);
            }

        }

        private static string platform_ref = null;

		[WebMethod]
		public void setPlatformRef(string arch_ref)
		{
            System.Net.ServicePointManager.DefaultConnectionLimit = 100;
            platform_ref = arch_ref;
		}

		[WebMethod]
		public string getPlatformRef()
		{
			return platform_ref;
		}

        /* component_ref é a referência do componente no sistema computacional (identificador do componente aninhado do componente de sistema).
		 * A plataforma já possui o componente de sistema previamente instanciado. 
		 */


		[WebMethod]
		public string instantiate(string component_ref, string[] ctree_str_list, int facet_instance, string[] channel_id, int[] channel_facet_instance, string[] channel_facet_address)
		{
			Console.WriteLine("PLATFORM INSTANTIATE START --- {0} on {1}", component_ref, platform_ref);

            try
            {
                Console.WriteLine("INSTANTIATE 1 -- " + platform_ref + " / " + component_ref);

                if (component_ref.Equals(platform_ref))
                {
                    Console.WriteLine("instantiate NEW --- reading system {0} {1}", component_ref, platform_ref);
                    string session_id_string = component_ref;
                    session = ParallelComputingSystem.newSystem(session_id_string, facet_instance);
                }
                else
                {
                    if (session == null)
                        throw new UninstantiatedPlatformException(platform_ref);

                    string ctree_str = ctree_str_list[0];

                    Console.WriteLine("instantiate NEW --- reading solution component {0} {1}", component_ref, platform_ref);
                    //Console.WriteLine("XML: {0}", ctree_str);
                    XMLComponentHierarchy.ComponentHierarchyType ctree_xml = LoaderApp.deserialize<XMLComponentHierarchy.ComponentHierarchyType>(ctree_str);
                    BackEnd.ResolveComponentHierarchy ctree = BackEnd.readComponentHierarchy(ctree_xml, component_ref);
                    ISystem system = ParallelComputingSystem.SystemInstance[session];
                    system.ChannelID = channel_id;
                    system.ChannelFacetInstance = channel_facet_instance;
                    system.ChannelFacetAddress = channel_facet_address;
                    system.newComponentInstance(component_ref, ctree);
                }
            }
            catch (HPCShelfException e)
            {
                Console.WriteLine("INSTANTIATE EXCEPTION (HPC Shelf exception)");

                throw WebServicesException.raiseSoapException(e, "VirtualPlatformServices.instantiate");
            }
            catch (SoapException e)
            {
                Console.WriteLine("INSTANTIATE EXCEPTION (soap exception)");
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine("INSTANTIATE EXCEPTION (exception)");

                Console.WriteLine(e.Message); Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message); Console.WriteLine(e.InnerException.StackTrace);
                }

                throw WebServicesException.raiseSoapException("HPCShelfWebServices",
                                                              "http://tempuri.org/HPCShelfWebServices",
                                                              e.Message,
                                                              ErrorCodes.GENERAL_ERROR.ToString(),
                                                              "VirtualPlatformServices.instantiate", WebServicesException.FaultCode.Server);
            }
            finally
            {
                Console.WriteLine("PLATFORM INSTANTIATE FINISH --- {0} on {1}", component_ref, platform_ref);
            }

            string result = "instantiated";

			return result;
		}


		private static BackEnd.ISession session = null;

        private static string status = "";

		[WebMethod]
		public string getStatus()
		{
			return status;
		}

        /*[WebMethod]
        public bool isRunning(string component_ref)
        {
            return BackEnd.isRunning(component_ref);
        }
        */
        [WebMethod]
        public void run (string component_ref)
        {
            try
            {
                status += platform_ref + ": * GO!!! BEGIN " + component_ref + "\n";

                BackEnd.runSolutionComponent(session.Services, component_ref);

                status += platform_ref + ": * GO!!! STARTED " + component_ref + "\n";
            }
            catch (HPCShelfException e)
            {
                throw WebServicesException.raiseSoapException(e, "VirtualPlatformServices.run");
            }
            catch (SoapException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message); Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message); Console.WriteLine(e.InnerException.StackTrace);
                }

                throw WebServicesException.raiseSoapException("HPCShelfWebServices",
                                                              "http://tempuri.org/HPCShelfWebServices",
                                                              e.Message,
                                                              ErrorCodes.GENERAL_ERROR.ToString(),
                                                              "VirtualPlatformServices.run", WebServicesException.FaultCode.Server);
            }
        }

        [WebMethod]
        public void release(string component_ref)
        {
            try
            {
                Console.WriteLine("RELEASE -- " + platform_ref + " / " + component_ref);

                if (session == null)
                    throw new UninstantiatedPlatformException(platform_ref);

                ISystem system = ParallelComputingSystem.SystemInstance[session];

                system.releaseComponentInstance(component_ref);
            }
            catch (HPCShelfException e)
            {
                throw WebServicesException.raiseSoapException(e, "VirtualPlatformServices.release");
            }
            catch (SoapException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message); Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message); Console.WriteLine(e.InnerException.StackTrace);
                }

                throw WebServicesException.raiseSoapException("HPCShelfWebServices",
                                                              "http://tempuri.org/HPCShelfWebServices",
                                                              e.Message,
                                                              ErrorCodes.GENERAL_ERROR.ToString(),
                                                              "VirtualPlatformServices.release", WebServicesException.FaultCode.Server);
            }
        }

        private static int next_port = -1;

		[WebMethod]
		public string allocateBindingAddress(string channel_id, int n)
		{
            if (next_port == -1)
				next_port = this.getBaseBindingPort();

            string binding_address_local = Environment.GetEnvironmentVariable("BINDING_ADDRESS_LOCAL");
			string binding_address_global = Environment.GetEnvironmentVariable("BINDING_ADDRESS_GLOBAL");

            string return_address = binding_address_local + ":" + next_port + ";" + binding_address_global + ":" + next_port;

			next_port += n*2;

			return return_address;

		}
	}
}

