#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <malloc.h>
#include <pthread.h>
#include "hpcshelf.h"

int ___rank = -1;
int ___facet = -1;
MPI_Comm parent_comm;

MPI_Comm* comm_port;

//int cs=0;

//pthread_mutex_t lock_all;

void take_id(HPCShelf_Port *port)
{
    if (___rank < 0)    
    {
      MPI_Comm_get_parent(&parent_comm);
      MPI_Comm_rank(parent_comm, &___rank);
      
      printf("taking rank (%d) and parent_communicator (%d)\n", ___rank, parent_comm);    
    }
    
    if (port != NULL && ___facet < 0)
    {
      int this_facet;
      HPCShelf_Get_facet(*port, &this_facet);
      
      int facet_count;
      HPCShelf_Get_facet_count(*port, &facet_count);

      int* facet_instance = (int*) malloc(facet_count * sizeof(int));
      HPCShelf_Get_facet_instance(*port, facet_instance);

      ___facet = facet_instance[this_facet];
      
      free(facet_instance);
      
      printf("taking facet (%d)\n", ___facet);
    }
}

pthread_mutex_t lock_send;

int HPCShelf_Send(void *buf,
	              int count,
	              MPI_Datatype datatype,
	              int facet,
	              int target,
	              int tag,
      	          HPCShelf_Port port)
{
    take_id(&port);

    // pthread_mutex_lock(&lock_all);

    if (facet != ___facet)
    {
        int datatype_size;
        MPI_Type_size(datatype, &datatype_size);
        
        printf("%d/%d: (HPCShelf_Send) DATA TYPE SIZE = %d\n", ___facet, ___rank, datatype_size);

        pthread_mutex_lock(&lock_send);
        
        int p[5] = { count, facet, target, datatype_size, tag /*, port*/ };
        MPI_Send(p, 5, MPI_INT, ___rank, 333, comm_port[port]);    
        
        if (facet > 0)
           MPI_Send(buf, count, datatype, ___rank, 444, comm_port[port]);
   }
   else
        MPI_Send(buf, count, datatype, ___rank, tag, comm_port[port]);

    pthread_mutex_unlock(&lock_send);
        
    // pthread_mutex_unlock(&lock_all);

    return 0;
}       
	   
pthread_mutex_t lock_recv;

int HPCShelf_Recv(void *buf,
	          int count,
		 	  MPI_Datatype datatype,
			  int facet,
			  int source,
			  int tag,
	          HPCShelf_Port port)
{
    take_id(&port);
    
    // pthread_mutex_lock(&lock_all);

   MPI_Status status;
   if (facet != ___facet)
    {
        int datatype_size;
        MPI_Type_size(datatype, &datatype_size);
        
        printf("%d/%d: (HPCShelf_Recv) DATA TYPE SIZE = %d\n", ___facet, ___rank, datatype_size);
        
        pthread_mutex_lock(&lock_recv);
        
        int p[5] = { count, facet, source, datatype_size, tag/*, port*/ };
        MPI_Send(p, 5, MPI_INT, ___rank, 555, comm_port[port]);    

        pthread_mutex_unlock(&lock_recv);

        // pthread_mutex_unlock(&lock_all);

       if (facet > 0)
        { 
            MPI_Recv(buf, count, datatype, ___rank, 666, comm_port[port], &status);
            
            int recv_count;
            MPI_Get_count(&status, datatype, &recv_count);
            if (recv_count != count)
            	printf("%d/%d: WRONG RECEIVED COUNT: %d X %d!!!! ******************\n", ___facet, ___rank, recv_count, count);
        }    
    }
    else
    {
        MPI_Recv(buf, count, datatype, ___rank, tag, comm_port[port], &status);
    }

    return 0;
}

pthread_mutex_t lock_browse;

int HPCShelf_Browse(const char *message)
{
    take_id(NULL);
    
    // pthread_mutex_lock(&lock_all);

    printf("%d/%d: ** %s **\n", ___facet > 0 ? ___facet : -1, ___rank, message); 
    
    char ranked_message[6 + strlen(message) + 1];
    sprintf(ranked_message, "%d/%d: %s", ___facet > 0 ? ___facet : -1, ___rank, message);
//    strcat(ranked_message, message);

    int message_size = strlen(ranked_message);

    pthread_mutex_lock(&lock_browse);
    MPI_Send(&message_size, 1, MPI_INT, ___rank, 111, parent_comm);
    MPI_Send(ranked_message, message_size, MPI_BYTE, ___rank, 222, parent_comm);
    pthread_mutex_unlock(&lock_browse);

    // pthread_mutex_unlock(&lock_all);
    
    return 0;
}


#define REQUEST_FACET 0 
#define REQUEST_FACET_COUNT 1
#define REQUEST_FACET_SIZE 2
#define REQUEST_FACET_INSTANCE 3
#define REQUEST_PORT 4
#define REQUEST_FINALIZE 5
#define REQUEST_INITIALIZE 6
#define REQUEST_FACET_WORLD 7
#define REQUEST_FACET_SIZE_WORLD 8 

pthread_mutex_t lock_get_facet_world;

int HPCShelf_Get_facet_world(int* facet)
{
    take_id(NULL);
    
    // pthread_mutex_lock(&lock_all);

    pthread_mutex_lock(&lock_get_facet_world);
    int v[2] = { REQUEST_FACET_WORLD, -1 };
    MPI_Send(v, 2, MPI_INT, ___rank, 777, parent_comm);

    MPI_Status status;    
    MPI_Recv(facet, 1, MPI_INT, ___rank, 999, parent_comm, &status);
    pthread_mutex_unlock(&lock_get_facet_world);
    
    // pthread_mutex_unlock(&lock_all);

    return 0;

}

pthread_mutex_t lock_get_facet_size_world;

int HPCShelf_Get_facet_size_world(int* facet_size)
{
    take_id(NULL);
    
    // pthread_mutex_lock(&lock_all);

    pthread_mutex_lock(&lock_get_facet_size_world);
    int v[2] = { REQUEST_FACET_SIZE_WORLD, -1 };
    MPI_Send(v, 2, MPI_INT, ___rank, 777, parent_comm);

    MPI_Status status;    
    MPI_Recv(facet_size, 1, MPI_INT, ___rank, 999, parent_comm, &status);
    pthread_mutex_unlock(&lock_get_facet_size_world);
    
    // pthread_mutex_unlock(&lock_all);

    return 0;
}

pthread_mutex_t lock_get_port;

int HPCShelf_Get_port(char* port_name, HPCShelf_Port* port)
{
    take_id(NULL);

    // pthread_mutex_lock(&lock_all);

    int length = strlen(port_name);
    int v[2] = { REQUEST_PORT, length };
    
    pthread_mutex_lock(&lock_get_port);
    MPI_Send(v, 2, MPI_INT, ___rank, 777, parent_comm);
    MPI_Send(port_name, length, MPI_CHAR, ___rank, 888, parent_comm);

    MPI_Comm parent_comm_dup;
    MPI_Comm_dup(parent_comm, &parent_comm_dup);

    MPI_Status status;    
    MPI_Recv(port, 1, MPI_INT, ___rank, 999, parent_comm, &status);

    comm_port[*port] = parent_comm_dup;
    
    printf("%d/%d: HPCShelf_Get_port (port=%d comm=%d)\n", ___facet, ___rank, *port, parent_comm_dup);

    pthread_mutex_unlock(&lock_get_port);
    
    // pthread_mutex_unlock(&lock_all);

    take_id(port);
        
    return 0;
}


void HPCShelf_Release_port(HPCShelf_Port port)
{
   int value=0;
   HPCShelf_Send(&value, 1, MPI_INT, -1, -1, 0, port);
   HPCShelf_Recv(&value, 1, MPI_INT, -1, -1, 0, port);
}


pthread_mutex_t lock_get_facet;

int HPCShelf_Get_facet(int key, int* facet)
{
    take_id(NULL);
    
    // pthread_mutex_lock(&lock_all);

    pthread_mutex_lock(&lock_get_facet);
    int v[2] = { REQUEST_FACET, key };
    MPI_Send(v, 2, MPI_INT, ___rank, 777, parent_comm);

    MPI_Status status;    
    MPI_Recv(facet, 1, MPI_INT, ___rank, 999, parent_comm, &status);
    pthread_mutex_unlock(&lock_get_facet);
    
    // pthread_mutex_unlock(&lock_all);

    return 0;
}

pthread_mutex_t lock_get_facet_count;

int HPCShelf_Get_facet_count(int key, int* facet_count)
{
    take_id(NULL);

    // pthread_mutex_lock(&lock_all);

    pthread_mutex_lock(&lock_get_facet_count);
    
    int v[2] = { REQUEST_FACET_COUNT, key };
    MPI_Send(v, 2, MPI_INT, ___rank, 777, parent_comm);

    MPI_Status status;    
    MPI_Recv(facet_count, 1, MPI_INT, ___rank, 999, parent_comm, &status);
    
    pthread_mutex_unlock(&lock_get_facet_count);
    
    // pthread_mutex_unlock(&lock_all);

    return 0;
}       

pthread_mutex_t lock_get_facet_size;

int HPCShelf_Get_facet_size(int key, int* facet_size)
{
    take_id(NULL);

    // pthread_mutex_lock(&lock_all);

    pthread_mutex_lock(&lock_get_facet_size);
    
    int v[2] = { REQUEST_FACET_SIZE, key };
    MPI_Send(v, 2, MPI_INT, ___rank, 777, parent_comm);

    MPI_Status status;
    
    int count;
    MPI_Recv(&count, 1, MPI_INT, ___rank, 888, parent_comm, &status);
    MPI_Recv(facet_size, count, MPI_INT, ___rank, 999, parent_comm, &status);
    
    pthread_mutex_unlock(&lock_get_facet_size);
    
    // pthread_mutex_unlock(&lock_all);

    return 0;
}       

pthread_mutex_t lock_get_facet_instance;

int HPCShelf_Get_facet_instance(int key, int* facet_instance)
{
    take_id(NULL);
    
    // pthread_mutex_lock(&lock_all);

    pthread_mutex_lock(&lock_get_facet_instance);
    
    int v[2] = { REQUEST_FACET_INSTANCE, key };
    MPI_Send(v, 2, MPI_INT, ___rank, 777, parent_comm);

    MPI_Status status;
    
    int count;
    MPI_Recv(&count, 1, MPI_INT, ___rank, 888, parent_comm, &status);
    MPI_Recv(facet_instance, count, MPI_INT, ___rank, 999, parent_comm, &status);
    
    pthread_mutex_unlock(&lock_get_facet_instance);
    
    // pthread_mutex_unlock(&lock_all);
    
    return 0;
}       

int HPCShelf_Init(int* argc, char **argv[])
{
	//pthread_mutex_init(&lock_all, NULL);
	pthread_mutex_init(&lock_send, NULL);
	pthread_mutex_init(&lock_recv, NULL);
	pthread_mutex_init(&lock_browse, NULL);
	pthread_mutex_init(&lock_get_port, NULL);
	pthread_mutex_init(&lock_get_facet, NULL);
	pthread_mutex_init(&lock_get_facet_count, NULL);                                   
	pthread_mutex_init(&lock_get_facet_size, NULL);                                   
	pthread_mutex_init(&lock_get_facet_instance, NULL);   	
	
    take_id(NULL);
	
    int v[2] = { REQUEST_INITIALIZE, 0 };
    MPI_Send(v, 2, MPI_INT, ___rank, 777, parent_comm);
	
    MPI_Status status;
    
    int* init_data = (int*) malloc(2*sizeof(int));
    MPI_Recv(init_data, 2, MPI_INT, ___rank, 999, parent_comm, &status);
    
    int port_count = init_data[0];
    int error = init_data[1];
    
    comm_port = (MPI_Comm*) malloc(port_count*sizeof(MPI_Comm));
    for (int i=0; i<port_count; i++)
        comm_port[i] = MPI_COMM_NULL;
        
    printf("%d/%d: HPCShelf_Initialize complete (port_count=%d)\n", ___facet, ___rank, port_count);
    
    if (error)
        printf("%d/%d: HPCShelf_Finalize error (%d).", ___facet, ___rank, error);

    free(init_data);

	return 0;                              
}

int HPCShelf_Finalize()
{
	//pthread_mutex_destroy(&lock_all);
	pthread_mutex_destroy(&lock_send);
	pthread_mutex_destroy(&lock_recv);
	pthread_mutex_destroy(&lock_browse);
	pthread_mutex_destroy(&lock_get_port);
	pthread_mutex_destroy(&lock_get_facet);
	pthread_mutex_destroy(&lock_get_facet_count);                                   
	pthread_mutex_destroy(&lock_get_facet_size);                                   
	pthread_mutex_destroy(&lock_get_facet_instance);   

    HPCShelf_Browse("done"); 

    int v[2] = { REQUEST_FINALIZE, 0 };
    MPI_Send(v, 2, MPI_INT, ___rank, 777, parent_comm);
	
    MPI_Status status;
    
    int error;
    MPI_Recv(&error, 1, MPI_INT, ___rank, 999, parent_comm, &status);
    
    if (error)
        printf("%d/%d: HPCShelf_Finalize error (%d).", ___facet, ___rank, error);
    
	return 0;                                
}


