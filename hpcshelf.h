#include "mpi.h"

typedef int HPCShelf_Port;

int HPCShelf_Init(int* argc, char **argv[]);
int HPCShelf_Finalize();

int HPCShelf_Get_facet_world(int* facet);
int HPCShelf_Get_facet_size_world(int* size);

int HPCShelf_Get_port(char* port_name, HPCShelf_Port* port);
int HPCShelf_Get_facet(HPCShelf_Port port, int* facet);
int HPCShelf_Get_facet_count(HPCShelf_Port port, int* facet_count);
int HPCShelf_Get_facet_size(HPCShelf_Port port, int* facet_size);
int HPCShelf_Get_facet_instance(HPCShelf_Port port, int* facet_instance);

void HPCShelf_Release_port(HPCShelf_Port port);

int HPCShelf_Browse(const char* message);

int HPCShelf_Send(void *buf, int count, MPI_Datatype datatype,
	              int facet, int rank, int tag, HPCShelf_Port port);

int HPCShelf_Recv(void *buf, int count, MPI_Datatype datatype, 
                  int facet, int rank, int tag, HPCShelf_Port port);
                  
