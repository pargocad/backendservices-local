#!/bin/sh

MAKE=make_PlatformServices.rsp
OLD="MONO_HOME"
NEW=$MONO_HOME

cp $MAKE temp.rsp
find ./ -iname temp.rsp | xargs sed -i --expression "s@$OLD@$NEW@"

if [ ! -d ./VirtualPlatformServices/bin ]
then
        mkdir ./VirtualPlatformServices/bin
fi

mcs @temp.rsp
rm temp.rsp
