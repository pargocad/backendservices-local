﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Protocols;
using org.hpcshelf.DGAC.utils;
using System.Runtime.CompilerServices;
using System.Collections.Concurrent;
using org.hpcshelf.exception;

namespace org.hpcshelf
{
    /* In the BackEnd server, the maintainer must do:
	 * 1) set the environment variable HPE_NEXT_FREE_WORKER_PORT
	 * 2) set the environment variable HPE_NEXT_FREE_PLATFORM_SERVICE_PORT
	 * 3) set the environment variable HPE_PLATFORM_ACCESS
	 * 4) copy the WorkerService.exe executable to $HOME/hpe_work/worker/bin
	 * 5) copy the platform service to $HOME/hpe_work/worker 
	 */


    public interface IBackendServices
    {
        string deploy(string platform_config);
        string release(string platform_address);
    }

    public class BackendServices : WebService, IBackendServices
    {
        private int first_base_binding_port = 12000;

        private static int count_p = 0;

        private string getVariable(string varName, string default_value)
        {
            if (Environment.GetEnvironmentVariable(varName) != null)
                return Environment.GetEnvironmentVariable(varName);
            else
                return default_value;
        }

        private static string mono_home_bin = Path.Combine(Environment.GetEnvironmentVariable("MONO_HOME"), "bin");
        private static string mpi_home_bin = Path.Combine(Environment.GetEnvironmentVariable("MPI_HOME"), "bin");

        [MethodImpl(MethodImplOptions.Synchronized)]
        private static string GetPublicIpAddress()
        {
            var request = (HttpWebRequest)WebRequest.Create("http://ifconfig.me");

            request.UserAgent = "curl"; // this will tell the server to return the information as if the request was made by the linux "curl" command

            string publicIPAddress;

            request.Method = "GET";
            using (WebResponse response = request.GetResponse())
            {
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    publicIPAddress = reader.ReadToEnd();
                }
            }

            return publicIPAddress.Replace("\n", "");
        }

        private static IDictionary<string, IList<int>> worker_ports = new ConcurrentDictionary<string, IList<int>>();

        private static string path_worker;
        private static string path_platform_service;

        private readonly static object lock_1 = new object();
        private readonly static object lock_2 = new object();
        private readonly static object lock_3 = new object();

        [WebMethod]
        public string deploy(string platform_config)
        {
            string virtual_platform_address = null;

            try
            {
                Console.WriteLine("platform_config is null ? {0}", platform_config == null);
                Console.WriteLine(platform_config);

                int nodes = int.Parse(platform_config) + 1;

                string home_path = Environment.GetEnvironmentVariable("HOME");

                int port_worker_base;
                lock (lock_1)
                {
                    // The next free communication port of a worker (it begins with 4865).
                    port_worker_base = int.Parse(getVariable("HPE_NEXT_FREE_WORKER_PORT", "4865"));
                    Environment.SetEnvironmentVariable("HPE_NEXT_FREE_WORKER_PORT", (port_worker_base + nodes).ToString());
                }

                int port_platform;
                lock (lock_2)
                {
                    // The next free communication port of a platform service (it begins witn 8081).
                    port_platform = int.Parse(getVariable("HPE_NEXT_FREE_PLATFORM_SERVICE_PORT", "8082"));
                    Environment.SetEnvironmentVariable("HPE_NEXT_FREE_PLATFORM_SERVICE_PORT", (port_platform + 1).ToString());
                }

                Console.WriteLine("port_worker_base of {1} is {0}", port_worker_base, port_platform);

                // The url of the platform (e.g. http://castanhao.lia.ufc.br).
                string platform_address = GetPublicIpAddress(); // getVariable("HPE_PLATFORM_ACCESS","127.0.0.1");                   Console.WriteLine("HPE_PLATFORM_ACCESS = {0}", platform_address);
                string platform_address_local = getVariable("HPE_PLATFORM_ACCESS_LOCAL", platform_address); Console.WriteLine("HPE_PLATFORM_ACCESS_LOCAL = {0}", platform_address_local);

                virtual_platform_address = "http://" + platform_address + ":" + port_platform + "/PlatformServices.asmx";

                path_worker = getVariable("MPIRUN_PATH", home_path);
                path_platform_service = getVariable("HPE_PATH_PLATFORM_SERVICE", Path.Combine(path_worker, "VirtualPlatformServices"));

                string backendservices_hpcshelf_home = Path.Combine(Environment.GetEnvironmentVariable("HPCShelf_HOME"), "BackendServices");

                string[] contents_configfile = new string[nodes];
                worker_ports[virtual_platform_address] = new List<int>();
                for (int i = 0; i < contents_configfile.Length; i++)
                {
                    int port = port_worker_base + i;
                    worker_ports[virtual_platform_address].Add(port);
//                    contents_configfile[i] = "-n 1 " + Path.Combine(mono_home_bin, "mono-service") + " -l:Worker" + (port_worker_base + i) + ".lock bin/WorkerService.exe --port " + (port_worker_base + i) + " --debug --no-deamon";
                  contents_configfile[i] = string.Format("-n 1 {0} -l:Worker{1}.lock {2} --port {1} --debug --no-deamon", Path.Combine(mono_home_bin, "mono-service"), port_worker_base + i, Path.Combine(backendservices_hpcshelf_home,"bin","WorkerService.exe"));
                    Console.WriteLine("LINE ******** {0}", contents_configfile[i]);
                }

                string configfile_name = Path.GetTempFileName();
                File.WriteAllLines(configfile_name, contents_configfile);

                Tuple<string, string>[] environment_w = new Tuple<string, string>[0];

                string[] contents_nodesfile = new string[nodes];
                for (int i = 0; i < contents_nodesfile.Length; i++)
                    contents_nodesfile[i] = platform_address_local + " " + (port_worker_base + i);

                string nodesfile_name = Path.GetTempFileName();
                File.WriteAllLines(nodesfile_name, contents_nodesfile);

                int count;
                lock (lock_3) count = count_p++;

                string BASE_BINDING_FACET_PORT = (first_base_binding_port + 1000 * count).ToString();

                Tuple<string, string>[] environment_p = new Tuple<string, string>[5];
                environment_p[0] = new Tuple<string, string>("PATH_HOSTS_FILE", nodesfile_name);
                environment_p[1] = new Tuple<string, string>("BASE_BINDING_FACET_PORT", BASE_BINDING_FACET_PORT);
                environment_p[2] = new Tuple<string, string>("BINDING_ADDRESS_LOCAL", platform_address_local);
                environment_p[3] = new Tuple<string, string>("BINDING_ADDRESS_GLOBAL", platform_address);
                environment_p[4] = new Tuple<string, string>("MPIRUN_PATH", path_worker);

                Console.WriteLine("BASE_BINDING_FACET_PORT {0} -- {1}", BASE_BINDING_FACET_PORT, virtual_platform_address);

                File.WriteAllText(Path.Combine(home_path, "workpeers"), "localhost");

                Thread tw = new Thread(launchWorker);
                Thread tp = new Thread(launchPlatformService);

                tw.Start(new Tuple<string, string, Tuple<string, string>[], string>(path_worker, configfile_name, environment_w, virtual_platform_address));
                tp.Start(new Tuple<string, int, Tuple<string, string>[], string>(path_platform_service, port_platform, environment_p, virtual_platform_address));
            }
            catch (HPCShelfException e)
            {
                virtual_platform_address = "exception";
                Console.WriteLine(e.Message); Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message); Console.WriteLine(e.InnerException.StackTrace);
                }
                throw WebServicesException.raiseSoapException(e, "BackendServices.deploy");
            }
            catch (SoapException e)
            {
                virtual_platform_address = "exception";
                Console.WriteLine(e.Message); Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message); Console.WriteLine(e.InnerException.StackTrace);
                }
                throw e;
            }
            catch (Exception e)
            {
                virtual_platform_address = "exception";
                Console.WriteLine(e.Message); Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message); Console.WriteLine(e.InnerException.StackTrace);
                }

                throw WebServicesException.raiseSoapException("HPCShelfWebServices",
                                                              "http://tempuri.org/HPCShelfWebServices",
                                                              e.Message,
                                                              ErrorCodes.GENERAL_ERROR.ToString(),
                                                              "BackendServices.deploy", WebServicesException.FaultCode.Server);
            }


            return virtual_platform_address;
        }

        private static IDictionary<string, Process> process_workers = new ConcurrentDictionary<string, Process>();
        private static IDictionary<string, Process> process_service = new ConcurrentDictionary<string, Process>();

        public BackendServices()
        {
            Console.WriteLine("BackendSErvices instances created !!! default connections={0}", ServicePointManager.DefaultConnectionLimit);
            ServicePointManager.DefaultConnectionLimit = 100;
        }

        private void launchWorker(object config_)
        {
            Tuple<string, string, Tuple<string, string>[], string> config = (Tuple<string, string, Tuple<string, string>[], string>)config_;
            string path_worker = config.Item1;
            string filename = config.Item2;
            Tuple<string, string>[] environment = config.Item3;
            string platform_adress = config.Item4;

            Console.WriteLine("mpirun on {0}", path_worker);
            var proc = CommandLineUtil.runCommandStart(Path.Combine(mpi_home_bin, "mpirun"), "--configfile " + filename, path_worker, environment);
            process_workers[platform_adress] = proc;

            try
            {
                CommandLineUtil.runCommandComplete(proc);
            }
            catch (Exception e)
            {
                Console.WriteLine("EXCEPTION: " + e.Message);
                if (e.InnerException != null)
                    Console.WriteLine("INNER EXCEPTION: " + e.InnerException.Message);
            }
            finally
            {
                process_workers.Remove(platform_adress);
            }


        }

        private void launchPlatformService(object config_)
        {
            Tuple<string, int, Tuple<string, string>[], string> config = (Tuple<string, int, Tuple<string, string>[], string>)config_;
            string path_platform_service = config.Item1;
            int port_platform = config.Item2;
            Tuple<string, string>[] environment = config.Item3;
            string platform_address = config.Item4;

            Console.WriteLine("XSP4 on {0}", path_platform_service);
            var proc = CommandLineUtil.runCommandStart(Path.Combine(mono_home_bin, "xsp4"), "--nonstop --port " + port_platform, path_platform_service, environment);
            process_service[platform_address] = proc;

            try
            {
                CommandLineUtil.runCommandComplete(proc);
            }
            catch (Exception e)
            {
                Console.WriteLine("EXCEPTION: " + e.Message);
                Console.WriteLine("STACK TRACE: " + e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine("INNER EXCEPTION: " + e.InnerException.Message);
                    Console.WriteLine("INNER STACK TRACE: " + e.InnerException.StackTrace);
                }
            }
            finally
            {
                process_service.Remove(platform_address);
            }
        }


        [WebMethod]
        public string release(string platform_address)
        {
            string return_message;

            Console.WriteLine("RELEASE: {0}", platform_address);

            try
            {
                IList<int> worker_port_list = worker_ports[platform_address];
                Process procw = process_workers[platform_address];
                Process procs = process_service[platform_address];

                procs.Kill();
                process_service.Remove(platform_address);

                string kill_command_args = "";
                foreach (int port in worker_port_list)
                    kill_command_args += int.Parse(File.ReadAllText(string.Format(Path.Combine(path_worker,"Worker{0}.lock"), port))) + " ";

                CommandLineUtil.runCommand("kill", kill_command_args, path_worker);
                worker_ports.Remove(platform_address);

                procw.Kill();
                process_workers.Remove(platform_address);

                return_message = string.Format("{0} released !", platform_address);
            }
            catch(Exception e)
            {
                Console.WriteLine("EXCEPTION: " + e.Message);
                Console.WriteLine("STACK TRACE: " + e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine("INNER EXCEPTION: " + e.InnerException.Message);
                    Console.WriteLine("INNER STACK TRACE: " + e.InnerException.StackTrace);
                }

                return_message = e.Message;
            }

            return return_message;
        }
    }
}

