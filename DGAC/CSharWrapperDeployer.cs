﻿

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using org.hpcshelf.DGAC.utils;
using DeployArguments;

public interface IDeployer
{
    string deploy(DeployArgumentsUnitType deploy_descriptor, byte[] key_file_contents);
}

public class CSharWrapperDeployer : IDeployer
{
    #region DEPLOYER

    /**
    * Salva arquivo fonte lido como um array de bytes em data, com o nome filename
    * em pasta definida pela classe Constants
    */
    [MethodImpl(MethodImplOptions.Synchronized)]
    public void saveData(byte[] data, string filename)
    {

        //saving source file
        if (data != null)
            FileUtil.saveByteArrayIntoFile(data, Path.Combine(Constants.PATH_TEMP_WORKER, filename + ".cs"));

    }

    /*
	 *Compila fonte salvo pelo método anterior. O resultado por ser uma DLL ou arquivo executável,
	 *dependendo do parâmetro outFile, definido pela classe CONSTANTS.
	 *Se o fonte resultar em uma dll, ela é instalada no GAC, na pasta "HASH"
	 *Se o fonte for um executável, então ele é salvo numa pasta temporária, definida pela 
	 *classe Constants 
	 */
    [MethodImpl(MethodImplOptions.Synchronized)]
    public string deploy(DeployArgumentsUnitType deploy_descriptor, byte[] key_file_contents)
    {
        string library_path = deploy_descriptor.library_path;  // component's qualified name
        string moduleName = deploy_descriptor.moduleName;      // interface's qualified name
        SourceContentsFile[] sourceContents = deploy_descriptor.sourceContents;
        string[] references = deploy_descriptor.references;

        //creates the strong key, for new assembly
        string publicKeyToken = create_strong_key(library_path, key_file_contents);

        //compile, generate dll 
        compile_source(sourceContents, library_path, moduleName, references);

        //installing on local GAC
        gacutil_install(library_path, moduleName);

        return publicKeyToken;
    }

	#endregion

	private static IDictionary<string, ReferenceType> externalReferences = FileUtil.loadExternalReferences();

	public static string create_strong_key(string nameWithoutExtension, byte[] key_file_contents)
	{
		String snkFileName = Path.Combine(Constants.PATH_TEMP_WORKER, nameWithoutExtension + ".backend.snk");

		Console.WriteLine("CREATING KEY FILE {0}", snkFileName);

		File.WriteAllBytes(snkFileName, key_file_contents);

		FileStream f = File.Open(snkFileName, FileMode.Open);

		StrongNameKeyPair rr = new StrongNameKeyPair(f);
		string skp = BitConverter.ToString(rr.PublicKey);
		f.Close();

		return skp.Replace("-", "");
	}

	public static void compile_source(DeployArguments.SourceContentsFile[] sourceContents,
									  string libraryPath,
									  string moduleName,
									  string[] references)
	{
		Console.WriteLine("Module NAME is " + moduleName);

		//references

		string mounted_references = "";
		if (references != null && references.Length > 0)
			foreach (string reference_ in references)
			{
				string reference = reference_;
				if (reference.StartsWith(":"))
				{
					reference = reference.Substring(1);
					// It is an external reference.
					ReferenceType pathRef;
					string path;
					if (externalReferences.TryGetValue(reference, out pathRef))
						path = pathRef.path;
					else
					{
						path = Path.Combine(Constants.PATH_GAC, reference);
						Console.Error.WriteLine("External reference " + reference + " not found. Using default " + path + ".");
					}
					reference = Path.Combine(path, reference + ".dll");
				}

				mounted_references += " -r:\"" + reference + (reference.EndsWith(".dll") ? "" : ".backend.dll") + "\"";
			}

		Console.WriteLine(mounted_references);

		// CREATE THE FILE <moduleName>.cs in the temporary directory with <contents> as the contents:

		string sourceCompileString = " ";
		foreach (DeployArguments.SourceContentsFile sourceContentsItem in sourceContents)
		{
			string fileSourceName = Path.Combine(Constants.PATH_TEMP_WORKER, libraryPath + "." + sourceContentsItem.fileName);

			File.WriteAllText(fileSourceName, sourceContentsItem.contents);

			sourceCompileString += fileSourceName + " ";
		}

        CommandLineUtil.runCommand(Constants.CS_COMPILER_PATH, Constants.cs_compiler_flags
					 + " -lib:" + Constants.PATH_DGAC + "," + Constants.PATH_GAC + " -r:DGAC.backend.dll"
					 + " /target:library /out:" + Path.Combine(Constants.PATH_TEMP_WORKER, moduleName + ".backend.dll")
					 + " /keyfile:" + Path.Combine(Constants.PATH_TEMP_WORKER, libraryPath + ".backend.snk")
					 + sourceCompileString
					 + mounted_references, null, null, null);
		// -r:mpibasicimpl\\IMPIBasicImpl.dll 
	}

	/// <summary>
	/// Method to install an assembly from gac
	/// assemblies were saved by worker in Constants.PATH_TEMP_WORKER
	/// assemblies will be installed in <gac_dir>/HASH wich must be in MONO_PATH
	/// </summary>
	/// <param name="assembly">The assembly name</param>
	/// <returns>bool</returns>
	public static void gacutil_install(string library_path, string assembly)
	{

		CommandLineUtil.runCommand(Constants.GACUTIL_PATH, "-u " + assembly + ".backend");
		CommandLineUtil.runCommand(Constants.GACUTIL_PATH, "-i " + Path.Combine(Constants.PATH_TEMP_WORKER, assembly + ".backend.dll") + " -package " + library_path, null, null, null);

		string package_path = Constants.PATH_GAC.Replace("\"", "");
		string fileSource = Path.Combine(Constants.PATH_TEMP_WORKER, assembly + ".backend.dll");
		string pathTarget = Path.Combine(package_path, library_path);
		string fileTarget = Path.Combine(pathTarget, assembly + ".backend.dll");
		if (!Directory.Exists(pathTarget))
		{
			Console.WriteLine("From " + fileSource + " to " + fileTarget);
			Directory.CreateDirectory(pathTarget);
		}
		File.Copy(fileSource, fileTarget, true);
   	}
}

public class CInteroperabilityDeployer : IDeployer
{
    public string deploy(DeployArgumentsUnitType deploy_descriptor, byte[] key_file_contents)
    {
        try
        {
            string library_path = deploy_descriptor.library_path;  // component's qualified name
            string moduleName = deploy_descriptor.moduleName;      // module name that is references in the C# wrapper code.
            SourceContentsFile[] sourceContents = deploy_descriptor.sourceContents;
            string[] references = deploy_descriptor.references;

            compile_source(sourceContents, library_path, moduleName, references);

            install_shared_library(library_path, moduleName);

            return moduleName;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            Console.WriteLine(e.StackTrace);
            if (e.InnerException != null)
            {
                Console.WriteLine(e.InnerException.Message);
                Console.WriteLine(e.InnerException.StackTrace);
            }
            throw e;
        }
    }

    // TODO: take references into consideration ...
    public static void compile_source(DeployArguments.SourceContentsFile[] sourceContents,
                                      string library_path,
                                      string module_name,
                                      string[] references)
    {


		string sourceCompileString = " ";
		foreach (DeployArguments.SourceContentsFile sourceContentsItem in sourceContents)
		{
			string fileSourceName = Path.Combine(Constants.PATH_TEMP_WORKER, sourceContentsItem.fileName);
			File.WriteAllText(fileSourceName, sourceContentsItem.contents);

			sourceCompileString += fileSourceName + " ";
		}
		
        string module_file_name = module_name + ".so";
        //string target_path = Path.Combine(Constants.PATH_GAC, library_path, module_file_name);

        string target_folder = Path.Combine(Constants.PATH_GAC, library_path);
        if (!File.Exists(target_folder))
            Directory.CreateDirectory(target_folder);

        string target_path = Path.Combine(target_folder, module_file_name);

        CommandLineUtil.runCommand(Constants.c_compiler, Constants.c_compiler_flags + " -fpic -shared -o " + target_path + " " + sourceCompileString, null, null, null);


					 /*+ " -lib:" + Constants.PATH_DGAC + "," + Constants.PATH_GAC + " -r:DGAC.backend.dll"
					 + " /target:library /out:" + Path.Combine(Constants.PATH_TEMP_WORKER, moduleName + ".backend.dll")
					 + " /keyfile:" + Path.Combine(Constants.PATH_TEMP_WORKER, libraryPath + ".backend.snk")
					 + sourceCompileString
					 + mounted_references, null, null, null);
*/


	}

    public static void install_shared_library(string library_path, string module_name)
    {
        // COPY the shared library to /usr/lib
        string module_file_name = module_name + ".so";

        string source_module_path = Path.Combine(Constants.PATH_GAC, library_path, module_file_name);

        // TODO ??? alternatively, modify LD_LIBRARY_PATH ...
        //string target_module_path = Path.Combine(Path.DirectorySeparatorChar,  "opt", "mono-4.2.2", "lib", "mono", "lib_c", module_file_name);

        if (!Directory.Exists(Path.Combine(Constants.PATH_GAC, "lib_c")))
            Directory.CreateDirectory(Path.Combine(Constants.PATH_GAC, "lib_c"));

        string target_module_path = Path.Combine(Constants.PATH_GAC, "lib_c", module_file_name);
        Console.WriteLine("install shared library  - target_module_path={0}", target_module_path);

        File.Copy(source_module_path, target_module_path, true);
    }

}

public class CMPIInteroperabilityDeployer : IDeployer
{
	public string deploy(DeployArgumentsUnitType deploy_descriptor, byte[] key_file_contents)
	{
		try
		{
			string library_path = deploy_descriptor.library_path;  // component's qualified name
			string moduleName = deploy_descriptor.moduleName;      // module name that is references in the C# wrapper code.
			SourceContentsFile[] sourceContents = deploy_descriptor.sourceContents;
			string[] references = deploy_descriptor.references;

			compile_source(sourceContents, library_path, moduleName, references);

			install_shared_library(library_path, moduleName);

			return moduleName;
		}
		catch (Exception e)
		{
			Console.WriteLine(e.Message);
			Console.WriteLine(e.StackTrace);
			if (e.InnerException != null)
			{
				Console.WriteLine(e.InnerException.Message);
				Console.WriteLine(e.InnerException.StackTrace);
			}
			throw e;
		}
	}

	// TODO: take references into consideration ...
	public static void compile_source(SourceContentsFile[] sourceContents,
									  string library_path,
									  string module_name,
									  string[] references)
	{
		string sourceCompileString = " ";
		foreach (SourceContentsFile sourceContentsItem in sourceContents)
		{
			string fileSourceName = Path.Combine(Constants.PATH_TEMP_WORKER, sourceContentsItem.fileName);
			File.WriteAllText(fileSourceName, sourceContentsItem.contents);
			sourceCompileString += fileSourceName + " ";
		}

		string module_file_name = module_name + ".so";

        string target_folder = Path.Combine(Constants.PATH_GAC, library_path);
        if (!File.Exists(target_folder))
            Directory.CreateDirectory(target_folder);

        string custom_flags_file = Path.Combine(target_folder, "flags");
        string custom_flags = File.Exists(custom_flags_file) ? File.ReadAllText(custom_flags_file) : "";

        string target_path = Path.Combine(target_folder, module_file_name);

		CommandLineUtil.runCommand(Constants.mpi_compiler, Constants.mpi_compiler_flags + " -fpic -shared -o " + target_path + " " + custom_flags + " " + sourceCompileString, null, null, null);
	}

	public static void install_shared_library(string library_path, string module_name)
	{
		// COPY the shared library to /usr/lib
		string module_file_name = module_name + ".so";

		string source_module_path = Path.Combine(Constants.PATH_GAC, library_path, module_file_name);

        // TODO ??? alternatively, modify LD_LIBRARY_PATH ...
        //string target_module_path = Path.Combine(Path.DirectorySeparatorChar,  "opt", "mono-4.2.2", "lib", "mono", "lib_c", module_file_name);

        if (!Directory.Exists(Path.Combine(Constants.PATH_GAC, "lib_c")))
            Directory.CreateDirectory(Path.Combine(Constants.PATH_GAC, "lib_c"));

        string target_module_path = Path.Combine(Constants.PATH_GAC, "lib_c", module_file_name);
        Console.WriteLine("install shared library  - target_module_path={0}", target_module_path);

        File.Copy(source_module_path, target_module_path, true);
	}
}

public class CPlusPlusInteroperabilityDeployer : IDeployer
{
    public string deploy(DeployArgumentsUnitType deploy_descriptor, byte[] key_file_contents)
    {
        try
        {
            string library_path = deploy_descriptor.library_path;  // component's qualified name
            string moduleName = deploy_descriptor.moduleName;      // module name that is references in the C# wrapper code.
            SourceContentsFile[] sourceContents = deploy_descriptor.sourceContents;
            string[] references = deploy_descriptor.references;

            compile_source(sourceContents, library_path, moduleName, references);

           // install_executable(library_path, moduleName);

            return moduleName;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            Console.WriteLine(e.StackTrace);
            if (e.InnerException != null)
            {
                Console.WriteLine(e.InnerException.Message);
                Console.WriteLine(e.InnerException.StackTrace);
            }
            throw e;
        }
    }

    // TODO: take references into consideration ...
    public static void compile_source(SourceContentsFile[] sourceContents,
                                      string library_path,
                                      string module_name,
                                      string[] references)
    {
        bool make_flag = false;
        string compiler_cmd = Constants.mpi_compiler;
        string compiler_flags = Constants.mpi_compiler_flags;
        string sourceCompileString = " ";
        foreach (SourceContentsFile sourceContentsItem in sourceContents)
        {
            string fileSourceName = Path.Combine(Constants.MPIRUN_PATH, sourceContentsItem.fileName);

            string pathFileSourceName = Path.GetDirectoryName(fileSourceName);
            Directory.CreateDirectory(pathFileSourceName);

            // string fileSourceName = Path.Combine(pathFileSourceName, sourceContentsItem.fileName);

            if (sourceContentsItem.fileType.Equals("binary"))
                File.WriteAllBytes(fileSourceName, Convert.FromBase64String(sourceContentsItem.contents));
            else
                File.WriteAllText(fileSourceName, sourceContentsItem.contents); 

            if (sourceContentsItem.fileType.Equals("make"))
                make_flag = true;

            string ext = Path.GetExtension(fileSourceName);
            if (ext.Equals(".cpp"))
            {
                compiler_cmd = Constants.mpicpp_compiler;
                compiler_flags = Constants.mpicpp_compiler_flags;
            }

            if (ext.Equals(".c") || ext.Equals(".cpp") || ext.Equals(".h"))
                sourceCompileString += fileSourceName + " ";
        }

        string target_folder = Constants.MPIRUN_PATH;
        //Directory.CreateDirectory(target_folder);

        string module_file_name = module_name + ".exe";
        string target_path = Path.Combine(target_folder, module_file_name);

        string lib_path = Path.Combine(Constants.PATH_GAC, "lib_c");

        string custom_flags_file = Path.Combine(target_folder, "flags");
        string custom_flags = File.Exists(custom_flags_file) ? File.ReadAllText(custom_flags_file) : "";

        if (make_flag)
        {
            CommandLineUtil.runCommand("make", "", target_folder);
        }
        else
            CommandLineUtil.runCommand(compiler_cmd, " -o " + target_path + " " + sourceCompileString + " -L" + lib_path + " " + compiler_flags + " " + custom_flags, null, null, null);
    }

  /*  public static void install_executable(string library_path, string module_name)
    {
        // COPY the shared library to /usr/lib
        string module_file_name = module_name + ".so";

        string source_module_path = Path.Combine(Constants.PATH_GAC, library_path, module_file_name);

        // TODO ??? alternatively, modify LD_LIBRARY_PATH ...
        //string target_module_path = Path.Combine(Path.DirectorySeparatorChar,  "opt", "mono-4.2.2", "lib", "mono", "lib_cpp", module_file_name);

        if (!Directory.Exists(Path.Combine(Constants.PATH_GAC, "lib_cpp")))
            Directory.CreateDirectory(Path.Combine(Constants.PATH_GAC, "lib_cpp"));

        string target_module_path = Path.Combine(Constants.PATH_GAC, "lib_c", module_file_name);
        Console.WriteLine("install  C++ shared library  - target_module_path={0}", target_module_path);

        File.Copy(source_module_path, target_module_path, true);
    }*/
}

public class PythonMPIInteroperabilityDeployer : IDeployer
{
    public string deploy(DeployArgumentsUnitType deploy_descriptor, byte[] key_file_contents)
    {
        try
        {
            string library_path = deploy_descriptor.library_path;  // component's qualified name
            string moduleName = deploy_descriptor.moduleName;      // module name that is references in the C# wrapper code.
            SourceContentsFile[] sourceContents = deploy_descriptor.sourceContents;
            string[] references = deploy_descriptor.references;
            
            install_py(library_path, moduleName, sourceContents);

            return moduleName;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            Console.WriteLine(e.StackTrace);
            if (e.InnerException != null)
            {
                Console.WriteLine(e.InnerException.Message);
                Console.WriteLine(e.InnerException.StackTrace);
            }
            throw e;
        }
    }

    public static void install_py(string library_path, string module_name, SourceContentsFile[] sourceContents)
    {
        foreach (SourceContentsFile sourceContentsItem in sourceContents)
        {
            string fileSourceName = Path.Combine(Constants.MPIRUN_PATH, sourceContentsItem.fileName);

            string pathFileSourceName = Path.GetDirectoryName(fileSourceName);
            Directory.CreateDirectory(pathFileSourceName);

            Console.WriteLine("install Python resource file -- target_module_path={0}", fileSourceName);

            if (sourceContentsItem.fileType.Equals("binary"))
                File.WriteAllBytes(fileSourceName, Convert.FromBase64String(sourceContentsItem.contents));
            else
                File.WriteAllText(fileSourceName, sourceContentsItem.contents);

        }
    }
}

public class JuliaMPIInteroperabilityDeployer : IDeployer
{
    public string deploy(DeployArgumentsUnitType deploy_descriptor, byte[] key_file_contents)
    {
        try
        {
            string library_path = deploy_descriptor.library_path;  // component's qualified name
            string moduleName = deploy_descriptor.moduleName;      // module name that is references in the C# wrapper code.
            SourceContentsFile[] sourceContents = deploy_descriptor.sourceContents;
            string[] references = deploy_descriptor.references;

            install_jl(library_path, moduleName, sourceContents);

            return moduleName;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            Console.WriteLine(e.StackTrace);
            if (e.InnerException != null)
            {
                Console.WriteLine(e.InnerException.Message);
                Console.WriteLine(e.InnerException.StackTrace);
            }
            throw e;
        }
    }

    public static void install_jl(string library_path, string module_name, SourceContentsFile[] sourceContents)
    {
        foreach (SourceContentsFile sourceContentsItem in sourceContents)
        {
            string fileSourceName = Path.Combine(Constants.MPIRUN_PATH, sourceContentsItem.fileName);

            string pathFileSourceName = Path.GetDirectoryName(fileSourceName);
            Directory.CreateDirectory(pathFileSourceName);

            Console.WriteLine("install Julia resource file -- target_module_path={0}", fileSourceName);

            if (sourceContentsItem.fileType.Equals("binary"))
                File.WriteAllBytes(fileSourceName, Convert.FromBase64String(sourceContentsItem.contents));
            else
                File.WriteAllText(fileSourceName, sourceContentsItem.contents);

        }
    }
}
