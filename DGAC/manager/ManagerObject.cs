﻿

//Remoting!
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting;
using System.Threading;
using org.hpcshelf.DGAC;
using org.hpcshelf.DGAC.utils;
using gov.cca;
using gov.cca.ports;

namespace org.hpcshelf.DGAC
{
    //MANAGER
    public class ManagerObject : MarshalByRefObject, gov.cca.Services,
                                                     gov.cca.AbstractFramework,
                                                     gov.cca.ports.BuilderService,
                                                     gov.cca.Component,
                                                     gov.cca.ports.ServiceRegistry
    {
        private PortUsageManager port_manager = new PortUsageManager();

        private static ManagerObject single_manager_object = null;

        public static ManagerObject SingleManagerObject
        {
            get
            {
                if (single_manager_object == null)
                    single_manager_object = new ManagerObject();
                return single_manager_object;
            }
        }

        private ManagerObject()
        {
            ServicePointManager.DefaultConnectionLimit = 100;

            Console.WriteLine("Manager Object UP ! {0}", ServicePointManager.DefaultConnectionLimit);
            instantiateWorkers();
            gov.cca.Services frw_services = getServices("manager_framework", "ManagerObject", new TypeMapImpl());
            Console.WriteLine("setServices BEFORE");
            this.setServices(frw_services);
            Console.WriteLine("setServices AFTER");
        }

        private IDictionary<string, ComponentID> componentIDs = new Dictionary<string, ComponentID>();
        private IDictionary<string, TypeMap> componentProperties = new Dictionary<string, TypeMap>();

        private IList<ConnectionID> connectionList = new List<ConnectionID>();
        private IDictionary<ConnectionID, TypeMap> connectionProperties = new Dictionary<ConnectionID, TypeMap>();
        private IDictionary<string, ConnectionID> connByUserPortName = new Dictionary<string, ConnectionID>();
        private IDictionary<string, ConnectionID> connByProviderPortName = new Dictionary<string, ConnectionID>();

        private IDictionary<string, ManagerServices> host_services = new Dictionary<string, ManagerServices>();
        private IDictionary<string, ManagerServices> component_services = new Dictionary<string, ManagerServices>();

        private IDictionary<string, Port> providesPorts = new Dictionary<string, Port>();
        private IDictionary<string, TypeMap> portProperties = new Dictionary<string, TypeMap>();
        private IDictionary<string, string> usesPortTypes = new Dictionary<string, string>();
        private IDictionary<string, string> providesPortTypes = new Dictionary<string, string>();

        private IDictionary<string, AutoResetEvent> waitingUserPorts = new Dictionary<string, AutoResetEvent>();

        private ManagerServices frw_services = null;
        private BuilderService frw_builder = null;

        public bool isConnected(ManagerComponentID cid, object portName)
        {
            ConnectionID conn;
            return connByUserPortName.TryGetValue(cid.getInstanceName() + ":" + portName, out conn);
        }


        #region AbstractFramework Members

        public TypeMap createTypeMap()
        {
            return new TypeMapImpl();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public gov.cca.Services getServices(string selfInstanceName, string selfClassName, TypeMap selfProperties)
        {
            int[] nodes = new int[WorkerFramework.Count];
            for (int i = 0; i < nodes.Length; i++)
                nodes[i] = i - 1;

            ManagerComponentID cid = new ManagerComponentIDImpl(selfInstanceName, selfClassName, nodes);
            ManagerServices manager_services = new ManagerServicesImpl(this, cid);

            // Register the host in the worker frameworks
            int j = 0;
            foreach (int i in WorkerFramework.Keys)
            {
                Console.WriteLine("BEFORE getServices ... 0");
                WorkerServices worker_service = (WorkerServices)WorkerFramework[i].getServices(selfInstanceName, selfInstanceName, selfProperties);

                Console.WriteLine("AFTER getServices ... 1 " + (worker_service == null));
                manager_services.registerWorkerService(j++, worker_service);
                Console.WriteLine("AFTER getServices ... 2");
            }

            this.registerComponent(cid, manager_services, selfProperties);
            this.registerHostService(cid, manager_services);

            return manager_services;
        }

        // private IDictionary<Services, ComponentRelease> releaseRegister = new Dictionary<Services, ComponentRelease>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void releaseServices(gov.cca.Services services)
        {
            ManagerServices services_ = (ManagerServices)services;
            this.host_services.Remove(services_.getComponentID().getInstanceName());
            for (int node = 0; node < WorkerFramework.Keys.Count; node++)
            {
                WorkerServices worker_services = services_.WorkerServices[node];
                WorkerFramework[node].releaseServices(worker_services);
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void shutdownFramework()
        {
            foreach (ManagerServices services in host_services.Values)
            {
                this.releaseServices(services);
            }

            foreach (IWorkerObject w in WorkerFramework.Values)
            {
                w.shutdownFramework();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public AbstractFramework createEmptyFramework()
        {
            return BackEnd.getFrameworkInstance();
        }

        #endregion

        private ServiceRegistry frw_registry = null;

        #region Component implementation
        public void setServices(gov.cca.Services services)
        {
            frw_services = (ManagerServices)services;

            // FETCH THE REGISTRY PORT TO DECLARE THE BUILDER SERVICE
            frw_services.registerUsesPort(Constants.REGISTRY_PORT_NAME, Constants.REGISTRY_PORT_TYPE, new TypeMapImpl());
            frw_registry = (ServiceRegistry)frw_services.getPort(Constants.REGISTRY_PORT_NAME);
            frw_registry.addSingletonService(Constants.BUILDER_SERVICE_PORT_TYPE, this);

            // FETCH THE BUILDER SERVICE
            frw_services.registerUsesPort(Constants.BUILDER_SERVICE_PORT_NAME, Constants.BUILDER_SERVICE_PORT_TYPE, new TypeMapImpl());
            frw_builder = (BuilderService)frw_services.getPort(Constants.BUILDER_SERVICE_PORT_NAME);

            int j = 0;
            foreach (int i in WorkerFramework.Keys)
            {
                WorkerServices worker_services = frw_services.WorkerServices[j++];
                ((gov.cca.Component)WorkerFramework[i]).setServices(worker_services);
            }
        }
        #endregion


        #region BuilderService Members


        [MethodImpl(MethodImplOptions.Synchronized)]
        public ComponentID createInstance(string instanceName, string className, TypeMap properties)
        {
            ManagerComponentID cid = null;
            try
            {
                string[] unit_ids;
                int[] indexes;
                int[] cid_nodes;
                int id_functor_app = default(int);
                WorkerComponentID[] worker_cids = null;


                string portName = (string)((TypeMapImpl)properties)[Constants.PORT_NAME];
                bool ignore = (bool)properties.getBool(Constants.IGNORE, false);
                int kind = properties.getInt(Constants.COMPONENT_KIND, 0);

                if (!ignore)
                {
                    this.createInstanceImpl(instanceName,
                                            className,
                                            (TypeMapImpl)properties,
                                            out cid_nodes,
                                            out unit_ids,
                                            out indexes,
                                            out worker_cids);

                    cid = new ManagerComponentIDImpl(instanceName,
                                                      className,
                                                     cid_nodes,
                                                     unit_ids,
                                                     indexes,
                                                     id_functor_app,
                                                     kind,
                                                      portName);
                }
                else
                {
                    id_functor_app = properties.getInt(Constants.ID_FUNCTOR_APP, 0);
                    kind = properties.getInt(Constants.COMPONENT_KIND, 0);

                    cid = new ManagerIgnoredComponentIDImpl(instanceName, className, id_functor_app, kind, portName);
                    Console.WriteLine("CREATE INSTANCE --- Ignoring " + instanceName);
                }

                ManagerServices cservices = new ManagerServicesImpl(this, cid);

                if (!ignore)
                {
                    int[] nodes = cid.WorkerNodes;
                    for (int i = 0; i < nodes.Length; i++)
                    {
                        WorkerComponentID wcid = worker_cids[i];

                        IWorkerObject worker_object = (IWorkerObject)WorkerFramework[nodes[i]];
                        RemoteWorkerServicesImpl worker_services = new RemoteWorkerServicesImpl(worker_object, wcid);
                        worker_object.linkToRemoteServices(worker_services.RemoteKey, wcid);

                        cservices.registerWorkerService(i, worker_services);
                    }
                }

                this.registerComponent(cid, cservices, properties);

            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
                Console.WriteLine("Inner Exception: " + (e.InnerException != null ? e.InnerException.Message : ""));
                Console.WriteLine("Stack Trace: " + e.StackTrace);
                throw e;
            }

            return cid;
        }

        // OK
        [MethodImpl(MethodImplOptions.Synchronized)]
        public ComponentID[] getComponentIDs()
        {
            ComponentID[] cids = new ManagerComponentIDImpl[componentIDs.Count];
            componentIDs.Values.CopyTo(cids, 0);
            return cids;
        }

        // OK
        [MethodImpl(MethodImplOptions.Synchronized)]
        public TypeMap getComponentProperties(ComponentID cid)
        {
            TypeMap properties;
            componentProperties.TryGetValue(cid.getInstanceName(), out properties);
            return properties;
        }

        // OK
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void setComponentProperties(ComponentID cid, TypeMap map)
        {
            if (componentProperties.ContainsKey(cid.getInstanceName()))
            {
                componentProperties.Remove(cid.getInstanceName());
            }

            componentProperties.Add(cid.getInstanceName(), map);
        }

        // OK
        [MethodImpl(MethodImplOptions.Synchronized)]
        public ComponentID getDeserialization(string s)
        {
            return null; /* TODO: XML serialization */
        }

        // OK
        [MethodImpl(MethodImplOptions.Synchronized)]
        public ComponentID getComponentID(string componentInstanceName)
        {
            ComponentID cid = null;
            componentIDs.TryGetValue(componentInstanceName, out cid);
            return cid;
        }

        public void checkThereAreConnections(ComponentID toDie)
        {
            string[] provider_ports = this.getProvidedPortNames(toDie);
            string[] user_ports = this.getUsedPortNames(toDie);

            foreach (string port_name in provider_ports)
            {
                ConnectionID conn;
                if (connByProviderPortName.TryGetValue(port_name, out conn))
                {
                    throw new CCAExceptionImpl("CCA Exception: the component instance cannot be destroyed because it has using connections.");
                }
            }

            foreach (string port_name in user_ports)
            {
                ConnectionID conn;
                if (connByUserPortName.TryGetValue(port_name, out conn))
                {
                    throw new CCAExceptionImpl("CCA Exception: the component instance cannot be destroyed because it has providing connections.");
                }
            }




            throw new CCAExceptionImpl("CCA Exception: Cannot destroy " + toDie.getInstanceName() + " because it has connections.");
        }

        // OK.
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void destroyInstance(ComponentID toDie, float timeout)
        {
            Console.WriteLine("BEGIN DESTROY INSTANCE #1 " + toDie.getInstanceName() + " frw_builder == null ? " + (frw_builder == null));

            // CALL DESTROY FOR EACH UNIT ...
            ManagerComponentID toDie_ = (ManagerComponentID)toDie;

            string[] usesPorts = frw_builder.getUsedPortNames(toDie);
            string[] providesPorts = frw_builder.getProvidedPortNames(toDie);

            Console.WriteLine("BEGIN DESTROY INSTANCE #2 " + toDie.getInstanceName());

            if (usesPorts.Length > 0 || providesPorts.Length > 0)
            {
                Console.WriteLine("The component must unregister its uses ports " +
                                                   "and remove provides ports before destruction.");
                foreach (string portName in usesPorts) Console.WriteLine("REMAINING USES PORT: " + portName);
                foreach (string portName in providesPorts) Console.WriteLine("REMAINING PROVIDES PORT " + portName);
                throw new CCAExceptionImpl(CCAExceptionType.Unexpected);
            }

            Console.WriteLine("BEGIN DESTROY INSTANCE #3 " + toDie.getInstanceName());

            int[] nodes = toDie_.WorkerNodes;

            // foreach (int i in toDie_.WorkerNodes)
            for (int i = 0; i < nodes.Length; i++)
            {
                Console.WriteLine("ENTERING DESTROY INSTANCE UNIT i=" + i);
                gov.cca.ports.BuilderService builder = this.WorkerBuilder[nodes[i]];
                WorkerComponentID wcid = toDie_.getWorkerComponentID(i);
                Console.WriteLine("ENTERING DESTROY INSTANCE UNIT wcid=" + wcid.getInstanceName());
                builder.destroyInstance(wcid, timeout);
                Console.WriteLine("EXIT DESTROY INSTANCE UNIT node=" + i + "wcid=" + wcid.getInstanceName());
            }

            Console.WriteLine("BEGIN DESTROY INSTANCE #4 " + toDie.getInstanceName());

            this.componentIDs.Remove(toDie.getInstanceName());

            Console.WriteLine("REMOVING component_services[" + toDie.getInstanceName() + "]");
            this.component_services.Remove(toDie.getInstanceName());
            if (host_services.ContainsKey(toDie.getInstanceName()))
                host_services.Remove(toDie.getInstanceName());
            this.componentProperties.Remove(toDie.getInstanceName());

            Console.WriteLine("END DESTROY INSTANCE" + toDie.getInstanceName());
        }



        // OK.
        [MethodImpl(MethodImplOptions.Synchronized)]
        public string[] getProvidedPortNames(ComponentID cid)
        {
            IDictionary<string, Tuple<int, int>[]> ports = this.getProvidedPorts(cid);
            string[] return_ports = new string[ports.Count];
            ports.Keys.CopyTo(return_ports, 0);
            return return_ports;
        }



        // OK
        public IDictionary<string, Tuple<int, int>[]> getProvidedPorts(ComponentID cid)
        {
            IDictionary<string, IList<Tuple<int, int>>> ports = new Dictionary<string, IList<Tuple<int, int>>>();
            ManagerComponentID cid_ = (ManagerComponentID)cid;

            int[] nodeList = cid_.WorkerNodes;
            for (int i = 0; i < nodeList.Length; i++)
            {
                WorkerComponentID wcid = cid_.getWorkerComponentID(i);
                BuilderService builder = this.WorkerBuilder[nodeList[i]];
                string[] portNames = builder.getProvidedPortNames(wcid);
                foreach (string portName in portNames)
                {
                    IList<Tuple<int, int>> node_indexes;
                    if (!ports.TryGetValue(portName, out node_indexes))
                    {
                        node_indexes = new List<Tuple<int, int>>();
                        ports[portName] = node_indexes;
                    }
                    node_indexes.Add(new Tuple<int, int>(nodeList[i], i));
                }
            }

            IDictionary<string, Tuple<int, int>[]> ports_ = new Dictionary<string, Tuple<int, int>[]>();
            foreach (KeyValuePair<string, IList<Tuple<int, int>>> p in ports)
            {
                Tuple<int, int>[] nodes = new Tuple<int, int>[p.Value.Count];
                p.Value.CopyTo(nodes, 0);
                Console.WriteLine("getProvidedPorts 2 p.Key={0}, nodes={1}", p.Key, nodes);
                ports_.Add(p.Key, nodes);
            }

            return ports_;
        }

        // OK
        [MethodImpl(MethodImplOptions.Synchronized)]
        public string[] getUsedPortNames(ComponentID cid)
        {
            IDictionary<string, Tuple<int, int>[]> ports = this.getUsedPorts(cid);
            string[] return_ports = new string[ports.Count];
            ports.Keys.CopyTo(return_ports, 0);
            return return_ports;
        }

        // OK
        public IDictionary<string, Tuple<int, int>[]> getUsedPorts(ComponentID cid)
        {
            Console.WriteLine("getUsedPorts BEGIN cid={0}", cid.getInstanceName());

            IDictionary<string, IList<Tuple<int, int>>> port_nodes = new Dictionary<string, IList<Tuple<int, int>>>();
            ManagerComponentID cid_ = (ManagerComponentID)cid;
            int[] nodeList = cid_.WorkerNodes;

            for (int i = 0; i < nodeList.Length; i++)
            {
                WorkerComponentID wcid = cid_.getWorkerComponentID(i);
                Console.WriteLine("getUsedPorts 0 node={1}, cid={0}", cid.getInstanceName(), nodeList[i]);
                BuilderService builder = WorkerBuilder[nodeList[i]];
                string[] portNames = builder.getUsedPortNames(wcid);
                foreach (string portName in portNames)
                {
                    Console.WriteLine("getUsedPorts 1 node={0}, wcid={1}, portName={2}", nodeList[i], wcid.getInstanceName(), portName);
                    IList<Tuple<int, int>> node_indexes;
                    if (!port_nodes.TryGetValue(portName, out node_indexes))
                    {
                        node_indexes = new List<Tuple<int, int>>();
                        port_nodes[portName] = node_indexes;
                    }
                    node_indexes.Add(new Tuple<int, int>(nodeList[i], i));
                }
            }

            IDictionary<string, Tuple<int, int>[]> ports_ = new Dictionary<string, Tuple<int, int>[]>();
            foreach (KeyValuePair<string, IList<Tuple<int, int>>> p in port_nodes)
            {
                Tuple<int, int>[] nodes = new Tuple<int, int>[p.Value.Count];
                p.Value.CopyTo(nodes, 0);
                Console.WriteLine("getUsedPorts 2 p.Key={0}, nodes={1}", p.Key, nodes);
                ports_.Add(p.Key, nodes);
            }

            Console.WriteLine("getUsedPorts END cid={0}", cid.getInstanceName());

            return ports_;
        }

        // OK
        [MethodImpl(MethodImplOptions.Synchronized)]
        public TypeMap getPortProperties(ComponentID cid, string portName)
        {
            HPETypeMap result = new TypeMapImpl();

            ManagerComponentID cid_ = (ManagerComponentID)cid;
            int[] nodes = cid_.WorkerNodes;
            //foreach (int node in list)
            for (int i = 0; i < nodes.Length; i++)
            {
                WorkerComponentID wcid = cid_.getWorkerComponentID(i);
                gov.cca.ports.BuilderService builder = this.WorkerBuilder[nodes[i]];
                HPETypeMap properties = (HPETypeMap)builder.getPortProperties(wcid, portName);

                foreach (KeyValuePair<string, object> p in properties)
                {
                    string key = p.Key;
                    object value = p.Value;

                    object o_;
                    IDictionary<int, object> o;
                    if (result.ContainsKey(key))
                    {
                        result.TryGetValue(key, out o_);
                        o = (IDictionary<int, object>)o_;
                    }
                    else
                    {
                        o = new Dictionary<int, object>();
                        result.Add(key, o);
                    }
                    o.Add(i, value);
                }
            }

            return result;
        }

        // OK
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void setPortProperties(ComponentID cid, string portName, TypeMap map)
        {
            ManagerComponentID cid_ = (ManagerComponentID)cid;
            int[] nodes = cid_.WorkerNodes;
            //foreach (int node in list)
            for (int i = 0; i < nodes.Length; i++)
            {
                WorkerComponentID wcid = cid_.getWorkerComponentID(i);
                gov.cca.ports.BuilderService builder = this.WorkerBuilder[nodes[i]];
                builder.setPortProperties(cid, portName, map);
            }
        }


        [MethodImpl(MethodImplOptions.Synchronized)]
        public ConnectionID connect(ComponentID user, string usingPortName, ComponentID provider, string providingPortName)
        {
            string usingPortNameQ = user.getInstanceName() + ":" + usingPortName;
            string providingPortNameQ = provider.getInstanceName() + ":" + providingPortName;

            Console.WriteLine("CONNECT {0}.{1} TO {2}.{3}", user.getInstanceName(), usingPortName, provider.getInstanceName(), providingPortName);

            ConnectionID connection = null;

            if (this.host_services.ContainsKey(user.getInstanceName()))
                connection = connect_h2c(user, usingPortName, provider, providingPortName);
            else
                connection = connect_c2c(user, usingPortName, provider, providingPortName);

            Console.WriteLine("CONNECT - STEP 1 -- {0}", usingPortNameQ);

            connectionList.Add(connection);
            //connByProviderPortName.Add(providingPortNameQ, connection);
            connByUserPortName.Add(usingPortNameQ, connection);
            port_manager.resetPort(usingPortNameQ);

            AutoResetEvent waiting_handle = null;
            if (waitingUserPorts.TryGetValue(usingPortNameQ, out waiting_handle))
                waiting_handle.Set();

            return connection;

        }

        private ConnectionID connect_h2c(ComponentID user,
                                          string usingPortName,
                                          ComponentID provider,
                                          string providingPortName)
        {
            Console.WriteLine("BEGIN CONNECT_H2C ");

            ConnectionID connection = null;

            ManagerComponentID mcid_provider = (ManagerComponentID)provider;
            ManagerComponentID mcid_user = (ManagerComponentID)user;

            if (!(mcid_provider is ManagerIgnoredComponentID))
            {
                int[] user_nodes = mcid_user.WorkerNodes;
                IDictionary<int, IList<WorkerComponentID>> user_nodes_inv = new Dictionary<int, IList<WorkerComponentID>>();
                //foreach (int node in node_user_list)
                for (int i = 0; i < user_nodes.Length; i++)
                {
                    IList<WorkerComponentID> user_cids_in_node;
                    if (!user_nodes_inv.TryGetValue(user_nodes[i], out user_cids_in_node))
                    {
                        user_cids_in_node = new List<WorkerComponentID>();
                        user_nodes_inv[user_nodes[i]] = user_cids_in_node;
                    }
                    user_cids_in_node.Add(mcid_user.getWorkerComponentID(i));
                }

                // GROUP BY NODES
                int[] provider_nodes = mcid_provider.WorkerNodes;
                WorkerConnectionID[] worker_connection_ = new WorkerConnectionID[provider_nodes.Length];

                IDictionary<int, IList<Tuple<WorkerComponentID, int, WorkerComponentID>>> worker_cid_list = new Dictionary<int, IList<Tuple<WorkerComponentID, int, WorkerComponentID>>>();
                for (int i = 0; i < provider_nodes.Length; i++)
                {
                    IList<Tuple<WorkerComponentID, int, WorkerComponentID>> worker_cids_of_node;
                    if (!worker_cid_list.TryGetValue(provider_nodes[i], out worker_cids_of_node))
                    {
                        worker_cids_of_node = new List<Tuple<WorkerComponentID, int, WorkerComponentID>>();
                        worker_cid_list[provider_nodes[i]] = worker_cids_of_node;
                    }
                    worker_cids_of_node.Add(new Tuple<WorkerComponentID, int, WorkerComponentID>(user_nodes_inv[provider_nodes[i]][0], i, mcid_provider.getWorkerComponentID(i)));
                }

                int node_count = -1;
                foreach (KeyValuePair<int, IList<Tuple<WorkerComponentID, int, WorkerComponentID>>> node in worker_cid_list)
                {
                    if (node.Key >= 0)
                    {
                        if (node.Value.Count == 1 && (node_count == -1 || node_count == 1))
                        {
                            Console.WriteLine("CONNECT H2C 1 node={0}", node.Key);
                            node_count = 1;
                            int i = node.Value[0].Item2;
                            gov.cca.ports.BuilderService wb = WorkerBuilder[node.Key];
                            WorkerComponentID ucid = node.Value[0].Item1;
                            WorkerComponentID pcid = node.Value[0].Item3;
                            worker_connection_[/*i*/ node.Key] = (WorkerConnectionID)wb.connect(ucid, usingPortName, pcid, providingPortName);
                        }
                        // Se há mais de dois, então devem ter wcids com nomes de instância diferentes.
                        else if (node.Value.Count > 1 && (node_count == -1 || node_count == node.Value.Count))
                        {
                            node_count = node.Value.Count;

                            Console.WriteLine("CONNECT H2C 2 node={0} instances={1}", node.Key, node_count);

                            foreach (Tuple<WorkerComponentID, int, WorkerComponentID> a in node.Value)
                            {
                                int i = a.Item2;
                                gov.cca.ports.BuilderService wb = WorkerBuilder[node.Key];
                                WorkerComponentID ucid = a.Item1;
                                WorkerComponentID pcid = a.Item3;
                                Console.WriteLine("CONNECT H2C 3 index={0} ucid={1} pcid={2} ", node.Key, ucid.getInstanceName(), pcid.getInstanceName());
                                worker_connection_[node.Key] = (WorkerConnectionID)wb.connect(ucid, usingPortName + "$" + i, pcid, providingPortName);
                                Console.WriteLine("CONNECT H2C 4 index={0} ucid={1} uport={3} pcid={2} pport={4}", node.Key, worker_connection_[i].getUser().getInstanceName(), worker_connection_[i].getProvider().getInstanceName(), worker_connection_[i].getUserPortName(), worker_connection_[i].getProviderPortName());
                            }
                        }
                        else
                        {
                            const string V = "connect_h2c (EXCEPTION): UNMATCHED ports ";
                            Console.WriteLine(V);
                            throw new Exception(V);
                        }
                    }
                }

                IList<WorkerConnectionID> worker_connection_list = new List<WorkerConnectionID>();
                for (int i = 0; i < worker_connection_.Length; i++)
                    if (worker_connection_[i] != null)
                        worker_connection_list.Add(worker_connection_[i]);

                WorkerConnectionID[] worker_connection = new WorkerConnectionID[worker_connection_list.Count];
                worker_connection_list.CopyTo(worker_connection, 0);

                connection = new ManagerConnectionIDImpl(provider, providingPortName, user, usingPortName, worker_connection);
            }
            else
            {
                connection = new ManagerConnectionIDImpl(provider, providingPortName, user, usingPortName);
                Console.WriteLine("connect_h2c -- IGNORE CONNECTION -- " + provider.getInstanceName());
            }

            Console.WriteLine("END CONNECT_H2C ");

            return connection;

        }

        private ConnectionID connect_c2c(ComponentID user,
                                          string usingPortName,
                                          ComponentID provider,
                                          string providingPortName)
        {
            ConnectionID connection = null;

            ManagerComponentID user_ = (ManagerComponentID)user;
            ManagerComponentID provider_ = (ManagerComponentID)provider;

            if (!(user is ManagerIgnoredComponentID) && !(provider is ManagerIgnoredComponentID))
            {
                IDictionary<Tuple<string, string>, Tuple<int, int>[]> used_ports = this.selectPorts(this.getUsedPorts(user_), usingPortName);
                IDictionary<Tuple<string, string>, Tuple<int, int>[]> provided_ports = this.selectPorts(this.getProvidedPorts(provider_), providingPortName);

                // ALIGN WORKER PORTS

                IDictionary<int, Tuple<IList<Tuple<string, int>>, IList<Tuple<string, int>>>> r = new Dictionary<int, Tuple<IList<Tuple<string, int>>, IList<Tuple<string, int>>>>();

                Console.WriteLine("\tconnect_c2c --- 0 --- {0}", used_ports.Count);

                foreach (KeyValuePair<Tuple<string, string>, Tuple<int, int>[]> p in used_ports)
                {
                    Console.WriteLine("connect_c2c --- 0 *** {0}", p.Value.Length);
                    foreach (Tuple<int, int> n in p.Value)
                    {
                        Console.WriteLine("connect_c2c --- 1 --- {0}", n.Item1);
                        if (n.Item1 >= 0)
                        {
                            Tuple<IList<Tuple<string, int>>, IList<Tuple<string, int>>> t;
                            if (!r.TryGetValue(n.Item1, out t))
                                t = new Tuple<IList<Tuple<string, int>>, IList<Tuple<string, int>>>(new List<Tuple<string, int>>(), new List<Tuple<string, int>>());
                            //   r[n.Item1].Item1.Add(new Tuple<string, int>(p.Key.Item2, n.Item2));
                            t.Item1.Add(new Tuple<string, int>(p.Key.Item2, n.Item2));
                            r[n.Item1] = t;
                        }
                    }
                }

                foreach (KeyValuePair<Tuple<string, string>, Tuple<int, int>[]> p in provided_ports)
                    foreach (Tuple<int, int> n in p.Value)
                        if (n.Item1 >= 0)
                        {
                            Console.WriteLine("connect_c2c ++++ {0}/{1},{2} ? {3}", p.Key.Item1, p.Key.Item2, n.Item1, r.ContainsKey(n.Item1));
                            r[n.Item1].Item2.Add(new Tuple<string, int>(p.Key.Item1, n.Item2));
                        }

                Console.WriteLine("connect_c2c --- 2");

                int[] provider_nodes = provider_.WorkerNodes;
                WorkerConnectionID[] worker_connection = new WorkerConnectionID[provider_nodes.Length];

                Console.WriteLine("connect_c2c --- 3");

                foreach (int node in r.Keys)
                    if (node >= 0)
                    {
                        Console.WriteLine("connect_c2c --- 4 --- {0}", node);

                        IList<Tuple<string, int>> port_user_list = r[node].Item1;
                        IList<Tuple<string, int>> port_prov_list = r[node].Item2;

                        if (port_prov_list.Count == 1)
                        {
                            WorkerComponentID cid_prov = provider_.getWorkerComponentID(port_prov_list[0].Item2);

                            foreach (Tuple<string, int> port_user in port_user_list)
                            {
                                WorkerComponentID cid_user = user_.getWorkerComponentID(port_user.Item2);
                                Console.WriteLine("CONNECT_C2C 1 -- begin -- {0}.{1} TO {2}.{3} node={4}", cid_user.getInstanceName(), port_user.Item1, cid_prov.getInstanceName(), providingPortName, node);
                                worker_connection[node] = (WorkerConnectionID)WorkerBuilder[node].connect(cid_user, port_user.Item1, cid_prov, providingPortName);
                                Console.WriteLine("CONNECT_C2C 1 -- end -- {0}.{1} TO {2}.{3}", cid_user.getInstanceName(), port_user.Item1, cid_prov.getInstanceName(), providingPortName);
                            }
                        }
                        else if (port_prov_list.Count == port_user_list.Count)
                        {
                            foreach (Tuple<string, int> port_user in port_user_list)
                            {
                                WorkerComponentID cid_user = user_.getWorkerComponentID(port_user.Item2);
                                foreach (Tuple<string, int> port_prov in port_prov_list)
                                    if (port_prov.Item1.EndsWith(port_user.Item1))
                                    {
                                        WorkerComponentID cid_prov = provider_.getWorkerComponentID(port_prov.Item2);
                                        Console.WriteLine("CONNECT_C2C 2 -- begin -- {0}.{1} TO {2}.{3} node={4}", cid_user.getInstanceName(), port_user.Item1, cid_prov.getInstanceName(), providingPortName, node);
                                        worker_connection[node] = (WorkerConnectionID)WorkerBuilder[node].connect(cid_user, port_user.Item1, cid_prov, providingPortName);
                                        Console.WriteLine("CONNECT_C2C 2 -- end -- {0}.{1} TO {2}.{3}", cid_user.getInstanceName(), port_user.Item1, cid_prov.getInstanceName(), providingPortName);
                                    }
                            }
                        }
                        else
                        {
                            Console.WriteLine("connect_c2c: unmatch user and provider --- user count = {0} / provider count = {1}", port_user_list.Count, port_prov_list.Count);
                            throw new Exception("connect_c2c: unmatch user and provider");
                        }
                    }
                connection = new ManagerConnectionIDImpl(provider, providingPortName, user, usingPortName, worker_connection);
            }
            else
            {
                connection = new ManagerConnectionIDImpl(provider, providingPortName, user, usingPortName);
                Console.WriteLine("connect_c2c -- IGNORE CONNECTION -- " + provider.getInstanceName());
            }

            return connection;
        }

        private IDictionary<Tuple<string, string>, Tuple<int, int>[]> selectPorts(IDictionary<string, Tuple<int, int>[]> port_list, string usingPortName)
        {
            IDictionary<Tuple<string, string>, Tuple<int, int>[]> output = new Dictionary<Tuple<string, string>, Tuple<int, int>[]>();

            foreach (KeyValuePair<string, Tuple<int, int>[]> p in port_list)
            {
                string[] w = p.Key.Split(new char[1] { ':' });
                Console.WriteLine("selectPorts --- w[0]={0}   w[1]={1}", w[0], w[1]);

                string port_name_q = w[1];
                string port_name = port_name_q.Split(new char[1] { '.' })[0];
                if (port_name.Equals(usingPortName))
                {
                    string worker_instance_name = w[0];
                    output[new Tuple<string, string>(worker_instance_name, port_name_q)] = p.Value;
                    Console.Write("selectPorts --- output[{0}/{1}]=", worker_instance_name, port_name_q);
                    foreach (Tuple<int, int> node in p.Value)
                        Console.Write("{0}/{1},", node.Item1, node.Item2);
                    Console.WriteLine(".");
                }
            }

            return output;
        }


        // OK
        [MethodImpl(MethodImplOptions.Synchronized)]
        public ConnectionID[] getConnectionIDs(ComponentID[] componentList)
        {
            Console.WriteLine("getConnectionIDs of {0} ({2}) -- connectionsList.Count = {1}", componentList[0].getInstanceName(), connectionList.Count, componentList[0].GetHashCode());

            IList<ComponentID> componentListList = new List<ComponentID>(componentList);

            IList<ConnectionID> connectionsFilter = new List<ConnectionID>();
            foreach (ConnectionID conn in connectionList)
            {
                ComponentID user = conn.getUser();
                ComponentID provider = conn.getProvider();

                Console.WriteLine("getConnectionIDs --- conn = {0} --- user={1} / provider={2}", conn, user.GetHashCode(), provider.GetHashCode());

                if (componentListList.Contains(user) || componentListList.Contains(provider))
                {
                    connectionsFilter.Add(conn);
                }
            }

            ConnectionID[] connectionArray = new ConnectionID[connectionsFilter.Count];
            connectionsFilter.CopyTo(connectionArray, 0);

            return connectionArray;
        }

        // OK
        [MethodImpl(MethodImplOptions.Synchronized)]
        public TypeMap getConnectionProperties(ConnectionID connID)
        {
            TypeMap properties = null;
            connectionProperties.TryGetValue(connID, out properties);
            return properties;
        }

        // OK
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void setConnectionProperties(ConnectionID connID, TypeMap map)
        {
            if (connectionProperties.ContainsKey(connID))
            {
                connectionProperties.Remove(connID);
            }
            connectionProperties.Add(connID, map);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void disconnect(ConnectionID connID, float timeout)
        {
            string userInstanceName = connID.getUser().getInstanceName();
            string usesPortName = userInstanceName + ":" + connID.getUserPortName();

            string providerInstanceName = connID.getProvider().getInstanceName();
            string providesPortName = providerInstanceName + ":" + connID.getProviderPortName();

            	Console.WriteLine ("MANAGER DISCONNECTING 1 " + usesPortName + " <- " + providesPortName);

            if (!port_manager.isReleased(usesPortName))
            {
                throw new CCAExceptionImpl(CCAExceptionType.UsesPortNotReleased);
            }

            	Console.WriteLine ("MANAGER DISCONNECTING 2 " + usesPortName + " <- " + providesPortName);

            if (!connectionList.Contains(connID))
            {
                throw new CCAExceptionImpl(CCAExceptionType.PortNotConnected);
            }

            	Console.WriteLine ("MANAGER DISCONNECTING 3 " + usesPortName + " <- " + providesPortName);

            ManagerComponentID cid_provider = (ManagerComponentID)connID.getProvider();
            ManagerComponentID cid_user = (ManagerComponentID)connID.getUser();

            if (!(cid_user is ManagerIgnoredComponentID) && !(cid_provider is ManagerIgnoredComponentID))
            {
                int[] nodes = cid_provider.WorkerNodes;
                for (int i = 0; i < nodes.Length; i++)
                    Console.WriteLine("MANAGER DISCONNECTING ....... nodes[{0}]={1}", i, nodes[i]);

                int j = 0;
                for (int i = 0; i < nodes.Length; i++)
                {
                    if (nodes[i] >= 0)
                    {
                        WorkerConnectionID conn_id = ((ManagerConnectionID)connID).getWorkerConnectionID(/*nodes[i]*/ j++);
                        Console.WriteLine("MANAGER DISCONNECTING 3.1 i=" + i + " - nodes[i]=" + nodes[i] + " conn_id is null ? " + (conn_id == null));

                        if (conn_id != null)
                            try
                            {
                                this.WorkerBuilder[nodes[i]].disconnect(conn_id, timeout);
                            }
                            catch (CCAExceptionImpl e)
                            {
                                Console.WriteLine("MANAGER DISCONNECTING 3.2 - EXCEPTION: " + e.getCCAExceptionType());
                                throw e;
                            }
                    }
                }
            }

            Console.WriteLine ("MANAGER DISCONNECTING 4 " + usesPortName + " <- " + providesPortName);

            connectionList.Remove(connID);
            connByUserPortName.Remove(usesPortName);
            connByProviderPortName.Remove(providesPortName);


            	Console.WriteLine ("MANAGER DISCONNECTING 5 " + usesPortName + " <- " + providesPortName);


            if (connectionProperties.ContainsKey(connID))
                connectionProperties.Remove(connID);

            	Console.WriteLine ("MANAGER DISCONNECTING 6 " + usesPortName + " <- " + providesPortName);

        }

        // OK
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void disconnectAll(ComponentID id1, ComponentID id2, float timeout)
        {
            if (id1 == null)
            {
                throw new CCAExceptionImpl(CCAExceptionType.BadPortName);
            }

            ManagerComponentID id1_m = (ManagerComponentID)id1;
            ManagerComponentID id2_m = (ManagerComponentID)id2;

            int[] id1_ws = id1_m.WorkerNodes;
            int[] id2_ws = id2_m.WorkerNodes;

            IDictionary<int, ComponentID> id2_ws_inv = new Dictionary<int, ComponentID>();
            for (int node = 0; node < id2_ws.Length; node++)
            {
                WorkerComponentID id2_w = id2_m.getWorkerComponentID(node);
                id2_ws_inv.Add(node, id2_w);
            }

            for (int node = 0; node < id1_ws.Length; node++)
            {
                WorkerComponentID id1_w = id1_m.getWorkerComponentID(node);
                ComponentID cid1 = id1_w;
                ComponentID cid2;
                if (id2_ws_inv.ContainsKey(node))
                {
                    id2_ws_inv.TryGetValue(node, out cid2);
                    gov.cca.ports.BuilderService builder = WorkerBuilder[id1_ws[node]];
                    builder.disconnectAll(cid1, cid2, timeout);
                }
                else
                {
                    throw new CCAExceptionImpl("Impossible to disconnect these components. They are not placed in the same set of nodes.");
                }
            }
        }

        #endregion



        public Port getServicePort(ComponentID user, string usedPortName)
        {
            string portType;
            if (usesPortTypes.TryGetValue(usedPortName, out portType))
            {
                Port singleton_service_port = null;
                ServiceProvider service_provider = null;
                if (usedPortName.EndsWith(Constants.REGISTRY_PORT_NAME))
                {
                    return this;
                }
                else if (this.provided_services_table.TryGetValue(portType, out service_provider))
                {
                    string providedPortName = service_provider.createService(portType);
                    ConnectionID conn = frw_builder.connect(user, usedPortName, frw_services.getComponentID(), providedPortName);

                    return getPortProceed(conn, usedPortName);
                }
                else if (this.singleton_provided_port_table.TryGetValue(portType, out singleton_service_port))
                {
                    return singleton_service_port;
                }
            }
            return null;
        }


        #region Services implementation

        public Port getPort(string portName)
        {
            Console.WriteLine("MANAGER OBJECT - getPort " + portName);
            ConnectionID conn = null;

            if (usesPortTypes.ContainsKey(portName))
            {
                if (connByUserPortName.TryGetValue(portName, out conn))
                {
                    return getPortProceed(conn, portName);
                }
                else
                {
                    Console.WriteLine("Wait for port " + portName);

                    AutoResetEvent wait_handle = new AutoResetEvent(false);
                    waitingUserPorts.Add(portName, wait_handle);
                    wait_handle.WaitOne();

                    connByUserPortName.TryGetValue(portName, out conn);
                    return getPortProceed(conn, portName);
                }
            }
            else
            {

                throw new CCAExceptionImpl(CCAExceptionType.PortNotDefined);
            }
        }

        public Port getPortNonblocking(string portName)
        {
            ConnectionID conn = null;
            if (connByUserPortName.TryGetValue(portName, out conn))
            {
                return getPortProceed(conn, portName);
            }
            else
            {
                throw new CCAExceptionImpl("ERROR: Port not connected - name=" + portName);
            }
        }

        public Port getPortProceed(ConnectionID conn, string user_port_name)
        {
            ManagerComponentID mcid = (ManagerComponentID)conn.getUser();
            ManagerComponentID wcid = (ManagerComponentID)conn.getProvider();
            ManagerServices ms = null;
            this.host_services.TryGetValue(mcid.getInstanceName(), out ms);

            IDictionary<int, WorkerServices> ws = ms.WorkerServices;
            int[] nodes = wcid.WorkerNodes;
            IList<gov.cca.Port> port_list = new List<gov.cca.Port>();

            foreach (int i in ws.Keys)
                Console.WriteLine("getPortProceed {0} *********************** ", i);

            int j = 0;
            for (int i = 0; i < nodes.Length; i++)
            {
               if (nodes[i] >= 0)
                {
                    WorkerConnectionID wconn = ((ManagerConnectionIDImpl)conn).getWorkerConnectionID(/*i*/j++);
                    Console.WriteLine("BEGIN GET PORT PROCEED {0} ", wconn.getUserPortName());
                    port_list.Add(ws[nodes[i] + 1].getPortNonblocking(wconn.getUserPortName())); // port[i] = ??
                    Console.WriteLine("END GET PORT PROCEED {0} ", wconn.getUserPortName());
                }
            }

            // gov.cca.Port[] ports = new gov.cca.Port[nodes.Length];
            gov.cca.Port[] ports = new gov.cca.Port[port_list.Count];
            port_list.CopyTo(ports, 0);

            //string user_port_name = conn.getUserPortName();
            gov.cca.Port port = null;

            /* TODO: It is still necessary to implement a general mechanism
             * for connecting to applications, where an application may provide
             * arbitrary ports for the host and vice-versa
             */
            if (user_port_name.EndsWith(Constants.GO_PORT_NAME))
            {
                port = new GoPortImpl(ms, ports);
            }
            else if (user_port_name.EndsWith(Constants.INITIALIZE_PORT_NAME))
            {
                port = new InitializePortImpl(ms, ports);
            }
            else if (user_port_name.EndsWith(Constants.RELEASE_PORT_NAME))
            {
                port = new ReleasePortImpl(ms, ports);
            }
            else if (user_port_name.EndsWith(Constants.RECONFIGURE_PORT_NAME))
            {
                port = new ReconfigurationAdvicePortImpl(ms, ports);
            }
            else
                throw new CCAExceptionImpl("CCA Exception (manager): Port not supported for host connections.");

            port_manager.addPortFetch(user_port_name);

            return port;

        }





        public void releasePort(string portName)
        {
            port_manager.addPortRelease(portName);

        }

        public void registerUsesPort(string portName, string type, gov.cca.TypeMap properties)
        {
            if (usesPortTypes.ContainsKey(portName))
            {
                throw new CCAExceptionImpl(CCAExceptionType.PortAlreadyDefined);
            }

            portProperties.Add(portName, properties);
            usesPortTypes.Add(portName, type);
        }

        public void unregisterUsesPort(string portName)
        {
            if (!usesPortTypes.ContainsKey(portName))
            {
                // It is a component uses port.
                //throw new CCAExceptionImpl(CCAExceptionType.PortNotDefined);
            }

            if (connByUserPortName.ContainsKey(portName))
            {
                throw new CCAExceptionImpl("CCA Exception (manager): it is not possible" +
                                           " to unregister a connected uses port." +
                                           " Try disconnect it first.");
            }

            portProperties.Remove(portName);
            usesPortTypes.Remove(portName);
            port_manager.resetPort(portName);
        }

        public void addProvidesPort(Port inPort, string portName, string type, gov.cca.TypeMap properties)
        {
            if (providesPorts.ContainsKey(portName))
            {
                throw new CCAExceptionImpl(CCAExceptionType.PortAlreadyDefined);
            }

            portProperties.Add(portName, properties);
            providesPortTypes.Add(portName, type);
            providesPorts.Add(portName, inPort);
            //throw new CCAExceptionImpl("Host programs cannot yet provide ports to applications.");
        }

        public gov.cca.TypeMap getPortProperties(string name)
        {
            if (!portProperties.ContainsKey(name))
            {
                throw new CCAExceptionImpl(CCAExceptionType.PortNotDefined);
            }

            TypeMap properties = null;
            portProperties.TryGetValue(name, out properties);
            return properties;
        }

        public void removeProvidesPort(string portName)
        {
            if (connByProviderPortName.ContainsKey(portName))
            {
                throw new CCAExceptionImpl("CCA Exception (manager): it is not possible" +
                                           " to unregister a connected provides port." +
                                           " Try disconnect it first.");
            }

            portProperties.Remove(portName);
            providesPortTypes.Remove(portName);
            providesPorts.Remove(portName);
        }

        public ComponentID getComponentID()
        {
            return frw_services.getComponentID();
        }

        private IList<ComponentRelease> callBackReleases = new List<ComponentRelease>();

        public void registerForRelease(ComponentRelease callBack)
        {
            callBackReleases.Add(callBack);
        }

        #endregion


        public ManagerServices getComponentServices(ManagerComponentID cid)
        {
            Console.WriteLine("getComponentServices " + cid.getInstanceName());
            foreach (string sss in component_services.Keys)
                Console.WriteLine("getComponentServices [" + sss + "]");
            //return (ManagerServices) component_services[cid.getInstanceName()];
            ManagerServices result = null;
            if (component_services.TryGetValue(cid.getInstanceName(), out result))
                return result;
            else
                return null;
        }

        private void registerComponent(ComponentID cid, ManagerServices services, TypeMap properties)
        {
            component_services.Add(cid.getInstanceName(), services);
            componentIDs.Add(cid.getInstanceName(), cid);
            componentProperties.Add(cid.getInstanceName(), properties);
        }

        private void registerHostService(ComponentID cid, ManagerServices services)
        {
            host_services.Add(cid.getInstanceName(), services);
        }


        public void resolve_topology(
                              Instantiator.UnitMappingType[] unit_mapping,
                              int facet_instance,
                              out string[] interface_ids,
                              out int[] nodes,
                              out string[] assembly_string,
                              out string[] class_name,
                              out string[][] port_name)
        {
            Console.WriteLine("BEGIN resolve_topology");

            interface_ids = null;
            nodes = null;

            Console.WriteLine("resolve_topology 1 " + (unit_mapping == null) + ", length=" + (unit_mapping.Length));

            int count_nodes;
            IDictionary<string, IList<Tuple<int, int[], int, string, string, string[]>>> unit_mapping_dict;

            // Note: only the units associated to the current facet are considered for instantiation.
            BackEnd.copy_unit_mapping_to_dictionary(unit_mapping, out unit_mapping_dict);
            BackEnd.count_nodes(facet_instance, unit_mapping, out count_nodes);

            Console.WriteLine("resolve_topology 2 - count_nodes = " + count_nodes);

            interface_ids = new string[count_nodes];
            nodes = new int[count_nodes];
            assembly_string = new string[count_nodes];
            class_name = new string[count_nodes];
            port_name = new string[count_nodes][];

            int i = 0;
            foreach (KeyValuePair<string, IList<Tuple<int, int[], int, string, string, string[]>>> um_item in unit_mapping_dict)
            {
                string id_interface = um_item.Key;
                IList<Tuple<int, int[], int, string, string, string[]>> node_list = um_item.Value;

                Console.WriteLine("resolve_topology #1");

                foreach (Tuple<int, int[], int, string, string, string[]> um in node_list)
                {
                    Console.WriteLine("resolve_topology #2.1 " + um.Item1 + "," + facet_instance);
                    if (um.Item1 == facet_instance || um.Item1 == -1)
                    {
                        Console.WriteLine("resolve_topology #2.2");

                        foreach (int node in um.Item2)
                        {
                            Console.WriteLine("resolve_topology #2.3");
                            Console.WriteLine("unit " + id_interface + " at node " + node);
                            interface_ids[i] = id_interface;
                            nodes[i] = node;
                            assembly_string[i] = um.Item4;
                          //  Console.WriteLine("resolve_topology : class_name = {0}", um.Item5);
                            class_name[i] = um.Item5;
                            port_name[i] = um.Item6 == null ? new string[0] : um.Item6;
                            i++;
                            Console.WriteLine("resolve_topology #2.4");
                        }

                    }
                    else
                    {
                        Console.WriteLine("Unit " + id_interface + " does not belong to facet instance {0}", facet_instance);
                    }
                }
                Console.WriteLine("resolve_topology #3");
            }
        }




        private void binding_calculate_ports_locally(int binding_sequential, int[] facet_access_port_base, ref int[] facet_access_port)
        {
            facet_access_port = new int[facet_access_port_base.Length];

            for (int i = 0; i < facet_access_port_base.Length; i++)
                facet_access_port[i] = facet_access_port_base[i] + binding_sequential * 256;
        }


        [MethodImpl(MethodImplOptions.Synchronized)]
        public int[] createInstanceImpl(
            string instanceName,
            string componentName,
            TypeMapImpl properties,
            out int[] cid_nodes,
            out string[] unit_ids,
            out int[] indexes,
            out WorkerComponentID[] worker_cids
            )
        {
            Console.WriteLine("CREATE INSTANCE IMPL #1");
            IList<int> cid_nodes_list = new List<int>();
            IList<string> unit_id_list_2 = new List<string>();
            IList<int> indexes_list = new List<int>();
            IList<WorkerComponentID> worker_cids_list = new List<WorkerComponentID>();

            try
            {
                Console.WriteLine("createInstance manager " + instanceName + ", " + componentName);

                int kind = properties.getInt(Constants.COMPONENT_KIND, 0);

                int this_facet_instance = (int)properties[Constants.FACET_INSTANCE]; Console.WriteLine("FACET_INSTANCE ok");

                Console.WriteLine("createInstanceImpl - this_facet_instance = " + this_facet_instance);

                IList<Tuple<string, string, string, string[]>>[] unit_data = null; ;
                int[] nodes = null;

                object unit_mapping = properties[Constants.UNIT_MAPPING];


                {
                    // MPMD case, also covering SPMD.
                    string[] unit_id_list_;
                    int[] nodes_;
                    string[] assembly_string_;
                    string[] class_name_;
                    string[][] port_name_;
                    resolve_topology((Instantiator.UnitMappingType[])unit_mapping,
                                     this_facet_instance,
                                     out unit_id_list_,
                                     out nodes_,
                                     out assembly_string_,
                                     out class_name_,
                                     out port_name_);

                    group_colocated_units(nodes_, unit_id_list_, assembly_string_, class_name_, port_name_, ref nodes, ref unit_data);
                }
                /* BINDING:
                   If the component is a binding, it is necessary to include the root unit, which is not referred in the instantiation file.
                   The root unit is instantiated in the master worker. */
                Console.WriteLine(kind);

                IDictionary<int, bool> node_marking = new Dictionary<int, bool>();
                foreach (int i in WorkerFramework.Keys) node_marking[i] = false;

                IDictionary<Thread, GoWorker> go_objs = new Dictionary<Thread, GoWorker>();
                IDictionary<Thread, int> thread_node = new Dictionary<Thread, int>();
                IDictionary<Thread, string> thread_unit_id = new Dictionary<Thread, string>();
                IDictionary<Thread, int> thread_index = new Dictionary<Thread, int>();

                IList<Thread> wthreads = new List<Thread>();

                for (int k = 0; k < nodes.Length; k++)
                {
                    Console.WriteLine(this_facet_instance + ": &&&***************** k=" + k + ", nodes[" + k + "]=" + nodes[k] + ", id_interface = " + unit_data[k].Count);
                }

                Instantiator.UnitMappingType[] unit_mapping_ = (Instantiator.UnitMappingType[])unit_mapping;
                foreach (Instantiator.UnitMappingType ooo in unit_mapping_)
                {
                    ooo.facetSpecified = true;
                    ooo.facet_instanceSpecified = true;
                }
                string unit_mapping_xml = LoaderApp.serialize<Instantiator.UnitMappingType[]>(unit_mapping_);
               // Console.WriteLine(this_facet_instance + ": UNIT MAPPING --- " + unit_mapping_xml);

                for (int k = 0; k < nodes.Length; k++)
                {
                    IList<Tuple<string, string, string, string[]>> id_unit_list = unit_data[k];
                    foreach (Tuple<string, string, string, string[]> tt in id_unit_list)
                    {
                        string id_unit = tt.Item1;
                        string assembly_string = tt.Item2;
                        string class_name = tt.Item3;
                        string[] port_name = tt.Item4;

                        Console.WriteLine(this_facet_instance + ": CREATE INSTANCE IMPL 1 " + id_unit_list.Count);

                        ///  DGAC.database.Unit u = DGAC.BackEnd.takeUnit(c, id_unit); 
                        //	AbstractComponentFunctor acf = DGAC.BackEnd.acfdao.retrieve(c.Id_abstract);

                        Console.WriteLine(this_facet_instance + ": CREATE INSTANCE IMPL 2 {0} ", k);

                        // SETUP PROPERTIES
                        TypeMapImpl worker_properties = new TypeMapImpl(properties);
                        worker_properties[Constants.KEY_KEY] = k;
                        worker_properties[Constants.COMPONENT_KEY] = componentName;
                        worker_properties[Constants.UNIT_KEY] = id_unit;
                        worker_properties[Constants.COMPONENT_KIND] = kind;
                        worker_properties[Constants.UNIT_MAPPING] = unit_mapping_xml;
                        worker_properties[Constants.ASSEMBLY_STRING_KEY] = assembly_string;
                        worker_properties[Constants.PORT_NAMES_KEY] = port_name;

                        Console.WriteLine(this_facet_instance + ": CREATE INSTANCE IMPL 3");

                        int[] facet_topology = (int[])properties[Constants.FACET_TOPOLOGY];

                        worker_properties[Constants.FACET_INSTANCE] = this_facet_instance;
                        worker_properties[Constants.FACET] = facet_topology[this_facet_instance];

                        // Inform to the root unit the communication addresses of the other binding facets. 
                        if (kind == Constants.KIND_BINDING)
                        {
                            Console.WriteLine(this_facet_instance + ": createInstanceImpl k=" + k + "- ENTERING BINDING --- instance_name" + componentName + ", id_unit=" + id_unit);

                            //if (k == 0)
                            if (nodes[k] < 0)
                            {
                                Console.WriteLine(this_facet_instance + ": CREATE INSTANCE IMPL 3.1");

                                worker_properties[Constants.CHANNEL_ID] = properties[Constants.CHANNEL_ID];
                                worker_properties[Constants.CHANNEL_FACET_INSTANCE] = properties[Constants.CHANNEL_FACET_INSTANCE];
                                worker_properties[Constants.CHANNEL_FACET_ADDRESS] = properties[Constants.CHANNEL_FACET_ADDRESS];
                            }
                        }

                        // INSTANTIATE THE UNITS ACCROSS THE WORKERS

                        Console.WriteLine(this_facet_instance + ": nodes[" + k + "]=" + nodes[k] + " / WorkerBuilder.Count = " + WorkerBuilder.Count);
                        foreach (int key in WorkerBuilder.Keys)
                            Console.WriteLine(this_facet_instance + ": key=" + key);

                        BuilderService worker = WorkerBuilder[nodes[k]];
                        GoWorker gw = new GoWorker(worker, instanceName + "." + id_unit, class_name, worker_properties);
                        Thread t = new Thread(gw.Run);
                        t.Start();
                        Console.WriteLine(this_facet_instance + ": START THREAD #" + k + " in " + nodes[k]);
                        wthreads.Add(t);
                        go_objs.Add(t, gw);
                        thread_node.Add(t, nodes[k]);
                        thread_unit_id.Add(t, id_unit);
                        thread_index.Add(t, 0);
                        node_marking[nodes[k]] = true;
                    }
                }

                Console.WriteLine(this_facet_instance + ": START JOIN 1");

                foreach (int i in node_marking.Keys)
                {
                    if (i > 0 && !node_marking[i])
                    {
                        Thread t = new Thread(((IWorkerObject)WorkerBuilder[i]).createInstanceNull);
                        wthreads.Add(t);
                        t.Start();
                        Console.WriteLine(this_facet_instance + ": START NULL THREAD #" + i);
                    }

                }
                Console.WriteLine(this_facet_instance + ": START JOIN 2");

                Exception e = null;

                foreach (Thread t in wthreads)
                {
                    t.Join();

                    if (go_objs.ContainsKey(t))
                    {
                        GoWorker go_obj;
                        go_obj = go_objs[t];
                        int node;
                        string unit_id;
                        int index;
                        node = thread_node[t];
                        unit_id = thread_unit_id[t];
                        index = thread_index[t];

                        if (go_obj.Exception != null)
                        {
                            Console.WriteLine("{0}: FAILED CREATE INSTANCE THREAD {1}, {2}[{3}]", this_facet_instance, node, unit_id, index);
                            e = go_obj.Exception;
                        }
                        else
                        {
                            WorkerComponentID wcid = go_obj.WorkerCID;
                            worker_cids_list.Add(wcid);
                            cid_nodes_list.Add(node);
                            unit_id_list_2.Add(unit_id);
                            indexes_list.Add(index);
                            Console.WriteLine("{0}: JOINED CREATE INSTANCE THREAD {1}, {2}[{3}]", this_facet_instance, node, unit_id, index);
                        }
                    }
                    else
                        Console.WriteLine(this_facet_instance + ": JOINED CREATE INSTANCE THREAD ");
                }
                if (e != null)
                {
                    Console.WriteLine("INSTANTIATE UNITS FAILED !!! \n EXCEPTION:{0}\n STACK TRACE: {1}", e.Message, e.StackTrace);
                    throw e;
                }


            }
            catch (Exception e)
            {
                throw e;
            }

            cid_nodes = new int[cid_nodes_list.Count];
            indexes = new int[indexes_list.Count];
            unit_ids = new string[unit_id_list_2.Count];
            worker_cids = new WorkerComponentID[worker_cids_list.Count];
            cid_nodes_list.CopyTo(cid_nodes, 0);
            indexes_list.CopyTo(indexes, 0);
            unit_id_list_2.CopyTo(unit_ids, 0);
            worker_cids_list.CopyTo(worker_cids, 0);

            return cid_nodes;
        }


        protected class GoWorker
        {
            private string instanceName = null;
            private string className = null;
            private TypeMap properties = null;
            private gov.cca.ports.BuilderService worker = null;
            private WorkerComponentID worker_cid = null;
            private Exception exception = null;
            public Exception Exception { get { return exception; } }

            public GoWorker(gov.cca.ports.BuilderService worker, string instanceName, string className, TypeMap properties)
            {
                this.instanceName = instanceName;
                this.className = className;
                this.properties = properties;
                this.worker = worker;
            }

            public void Run()
            {
                try
                {
                   // Console.WriteLine("Calling worker. Instantiating " + instanceName + " " + className + " null ? " + (worker == null));
                    worker_cid = (WorkerComponentID)worker.createInstance(instanceName, className, properties);
                }
                catch (Exception e)
                {
                    exception = e;
                }
            }

            public WorkerComponentID WorkerCID
            {
                get
                {
                    return worker_cid;
                }
            }
        }

        private void group_colocated_units(int[] nodes_, string[] interface_ids_, string[] assembly_string_, string[] class_name_, string[][] port_name_, ref int[] nodes, ref IList<Tuple<string, string, string, string[]>>[] interface_ids)
        {
            IDictionary<int, IList<Tuple<string, string, string, string[]>>> nodes_dict = new Dictionary<int, IList<Tuple<string, string, string, string[]>>>();
            for (int k = 0; k < nodes_.Length; k++)
            {
                int node = nodes_[k];
                string id_interface = interface_ids_[k];
                string assembly_string = assembly_string_[k];
                string class_name = class_name_[k];
                string[] port_name = port_name_[k];

                IList<Tuple<string, string, string, string[]>> interface_id_list;
                if (!nodes_dict.TryGetValue(node, out interface_id_list))
                {
                    interface_id_list = new List<Tuple<string, string, string, string[]>>();
                    nodes_dict[node] = interface_id_list;
                }

                interface_id_list.Add(new Tuple<string, string, string, string[]>(id_interface, assembly_string, class_name, port_name));
            }
            int i = 0;
            nodes = new int[nodes_dict.Count];
            interface_ids = new IList<Tuple<string, string, string, string[]>>[nodes_dict.Count];
            foreach (KeyValuePair<int, IList<Tuple<string, string, string, string[]>>> nodes_item in nodes_dict)
            {
                nodes[i] = nodes_item.Key;
                interface_ids[i] = nodes_item.Value;
                i++;
            }

            for (int j = 0; j < nodes.Length; j++)
                foreach (Tuple<string, string, string, string[]> interface_id in interface_ids[j])
                    Console.WriteLine("group_colocated_units: j={0} / nodes[{0}]=" + nodes[j] + " / interface_ids[{0}]=" + interface_id.Item1, j);

        }




        /* The Worker Object of each computing node */
        private IDictionary<int, gov.cca.AbstractFramework> worker_framework = null;

        public IDictionary<int, gov.cca.AbstractFramework> WorkerFramework
        {
            get
            {
                if (worker_framework == null)
                {
                    instantiateWorkers();
                }
                return worker_framework;
            }
        }


        private IDictionary<int, gov.cca.ports.BuilderService> worker_builder = null;

        private IDictionary<int, gov.cca.ports.BuilderService> WorkerBuilder
        {
            get
            {
                if (worker_builder == null)
                {
                    this.connectToWorkerBuilders();
                }
                return worker_builder;
            }
        }


        private void connectToWorkerBuilders()
        {
            worker_builder = new Dictionary<int, BuilderService>(); // BuilderService[WorkerFramework.Length];

            int j = 0;
            foreach (int node in WorkerFramework.Keys)
            {
                Services srv = this.frw_services.WorkerServices[j++];
                worker_builder[node] = (srv.getPort(Constants.BUILDER_SERVICE_PORT_NAME) as BuilderService);
                Console.WriteLine("connectToWorkerBuilders - node=" + node + " worker_builder[node] = " + worker_builder[node].GetHashCode());
            }

        }

        private void readNodesFile(out string node_master, out int port_master, out IList<string> nodeList, out IList<int> portList)
        {
            node_master = null;
            port_master = -1;

            nodeList = new List<string>();
            portList = new List<int>();

            Console.WriteLine("HOST FILE : " + Constants.HOSTS_FILE);

            using (TextReader tr = new StreamReader(Constants.HOSTS_FILE))
            {
                string one_line;
                if ((one_line = tr.ReadLine()) != null)
                {
                    int my_port = Constants.WORKER_PORT;
                    string[] s = one_line.Split(' ');
                    if (s.Length > 1)
                        my_port = System.Convert.ToInt32(s[1], 10);
                    node_master = s[0];
                    port_master = my_port;
                }
                while ((one_line = tr.ReadLine()) != null)
                {
                    int my_port = Constants.WORKER_PORT;
                    string[] s = one_line.Split(' ');
                    if (s.Length > 1)
                        my_port = System.Convert.ToInt32(s[1], 10);
                    nodeList.Add(s[0]);
                    portList.Add(my_port);
                }
                //node = new string[nodeList.Count];
                //port = new int[portList.Count];
                //nodeList.CopyTo(node, 0);
                //portList.CopyTo(port, 0);
            }

        }


        private int failsCount = 0;

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void instantiateWorkers()
        {
            Console.WriteLine("Starting worker clients !");

            string node_master = null;
            IList<string> node = null;

            try
            {
                /* Read nodes file, and fill the node array */

                int port_master;
                IList<int> port;

                readNodesFile(out node_master, out port_master, out node, out port);

                Console.WriteLine("READ NODE FILES - " + node.Count + "," + port.Count + "," + node_master + "," + port_master);

                worker_framework = new Dictionary<int, gov.cca.AbstractFramework>();

                // CONNECT TO THE ROOT WORKER 

                try
                {
                    worker_framework[-1] = BackEnd.getFrameworkWorkerInstanceWCF(node_master, port_master, 0);
                    Console.WriteLine("CONNECTED TO MASTER in {0} : {1} --- {2}", node_master, port_master, worker_framework[-1].GetHashCode());
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine("ERROR CONNECTING TO MASTER in " + node_master + ":" + port_master);
                    Console.Error.WriteLine("EXCEPTION: " + e.Message);
                    Console.Error.WriteLine("INNER EXCEPTION: " + e.InnerException.Message);
                    failsCount++;
                }
                finally
                {
                }

                int i = 0;
                // CREATE THE COMPUTATION WORKERS
                /* Create each worker object and fill the worker array */

                foreach (string n in node)
                {
                    try
                    {
                        worker_framework[i] = BackEnd.getFrameworkWorkerInstanceWCF(n, port[i], i + 1);

                        Console.WriteLine("CONNECTED TO WORKER #{0} in {1} : {2} --- {3}", i, n, port[i], worker_framework.GetHashCode());
                    }
                    catch (RemotingException e)
                    {
                        Console.Error.WriteLine("ERROR CONNECTING TO WORKER #" + i + " in " + n + ":" + port[i]);
                        //Console.WriteLine(e.InnerException.Message);
                        failsCount++;
                    }
                    finally
                    {
                        i++;
                    }
                }


                IList<Thread> threads = new List<Thread>();
                foreach (AbstractFramework wf in WorkerFramework.Values)
                {
                    IWorkerObject wfo = (IWorkerObject)wf;
                    Thread t1 = new Thread(wfo.setUpCommunicationScope);
                    threads.Add(t1);
                }

                foreach (Thread t in threads) t.Start();
                foreach (Thread t in threads) t.Join();
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("EXCEPTION: " + e.Message);
                Console.Error.WriteLine("INNER EXCEPTION: " + e.InnerException.Message);
                Console.Error.WriteLine("STACK TRACE: " + e.StackTrace);
                throw e;
            }

            Console.WriteLine(WorkerFramework.Count + " Worker clients started !");
        }



        #region ServiceRegistry implementation
        private IDictionary<string, ServiceProvider> provided_services_table = new Dictionary<string, ServiceProvider>();
        private IDictionary<string, Port> singleton_provided_port_table = new Dictionary<string, Port>();

        public bool addService(string serviceType, ServiceProvider portProvider)
        {
            if (!provided_services_table.ContainsKey(serviceType))
            {
                provided_services_table.Add(serviceType, portProvider);
                return true;
            }
            else
                return false;
        }

        public bool addSingletonService(string serviceType, Port server)
        {
            if (!singleton_provided_port_table.ContainsKey(serviceType))
            {
                singleton_provided_port_table.Add(serviceType, server);
                return true;
            }
            else
                return false;
        }

        public void removeService(string serviceType)
        {
            if (provided_services_table.ContainsKey(serviceType))
            {
                provided_services_table.Remove(serviceType);
            }
            else if (singleton_provided_port_table.ContainsKey(serviceType))
            {
                singleton_provided_port_table.Remove(serviceType);
            }
            else
                throw new CCAExceptionImpl("CCA Exception (Manager.removeService): there is another service registered for type " + serviceType);

        }

        #endregion


    }


}
