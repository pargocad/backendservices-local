﻿using System;
using org.hpcshelf.unit;

namespace org.hpcshelf.kinds
{


    public interface IFacetKind : IUnit
    {
    }

    public abstract class Facet : Unit, IFacetKind
    {

    }

}
