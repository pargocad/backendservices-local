﻿/*=============================================================
(c) all rights reserved
================================================================*/

using gov.cca;
using org.hpcshelf.unit;
using System.ServiceModel;
using gov.cca.ports;

namespace org.hpcshelf.ports
{
	[ServiceContract]
	[ServiceKnownType(typeof(InitializePortWrapper))]
	public interface MultipleGoPort : GoPort
	{
        void addPort(GoPort port);
	}
}
