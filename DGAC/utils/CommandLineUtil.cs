﻿using System;
using System.Diagnostics;
using System.IO;

namespace org.hpcshelf.DGAC.utils
{

    public class CommandLineUtil
    {
        public static int runCommandComplete(System.Diagnostics.Process proc)
        {
            int ExitCode = -1;
            try
            {
                proc.WaitForExit();

                ExitCode = proc.ExitCode;
                proc.Close();

                if (ExitCode > 0)
                {
                    string message = "Error executing command: " + proc.StartInfo.FileName + " " + proc.StartInfo.Arguments + "\n" + output_str;
                    throw new Exception(message);
                }
            }
            catch (System.ComponentModel.Win32Exception w)
            {
                Console.WriteLine("Message: " + w.Message);
                Console.WriteLine("ErrorCode: " + w.ErrorCode.ToString());
                Console.WriteLine("NativeErrorCode: " + w.NativeErrorCode.ToString());
                Console.WriteLine("StackTrace: " + w.StackTrace);
                Console.WriteLine("Source: " + w.Source);
                Exception e = w.GetBaseException();
                Console.WriteLine("Base Exception Message: " + e.Message);

                throw w;
            }
            finally
            {
            }
            return ExitCode;
        }

        public static System.Diagnostics.Process runCommandStart(string cmd, string args, string userName, string password_, String curDir, Tuple<string, string>[] environment)
        {
            int ExitCode;

            System.Security.SecureString password = null;

            if (password_ != null)
            {
                password = new System.Security.SecureString();
                foreach (char c in password_)
                    password.AppendChar(c);
                password.MakeReadOnly();
            }

            output_str = "";

            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.EnableRaisingEvents = false;
            proc.StartInfo.CreateNoWindow = false; //true
            proc.StartInfo.UseShellExecute = false;
            foreach (Tuple<string, string> env in environment)
                proc.StartInfo.EnvironmentVariables.Add(env.Item1, env.Item2);
            proc.StartInfo.FileName = cmd;
            proc.StartInfo.Arguments = args;
            proc.StartInfo.RedirectStandardError = false; //true;
            proc.StartInfo.RedirectStandardOutput = false; // true;
                                                           //proc.ErrorDataReceived += new DataReceivedEventHandler(OutputHandler);
                                                           //proc.OutputDataReceived += new DataReceivedEventHandler(OutputHandler);
            if (userName != null) proc.StartInfo.UserName = userName;
            if (password != null) proc.StartInfo.Password = password;
            if (curDir != null)
            {
                string homeDir = System.Environment.GetEnvironmentVariable("HOME");
                if (homeDir != null)
                {
                    proc.StartInfo.WorkingDirectory = Path.Combine(homeDir, curDir);
                }
                else
                {
                    proc.StartInfo.WorkingDirectory = curDir;
                }
            }

            Console.WriteLine(userName + " runs " + cmd + args + " on " + curDir);

            proc.Start();

            //proc.BeginErrorReadLine();
            //proc.BeginOutputReadLine();

            return proc;
        }

        public static System.Diagnostics.Process runCommand(string cmd, string args)
        {
            return runCommand(cmd, args, null, null, null);
        }

        public static System.Diagnostics.Process runCommand(string cmd, string args, Tuple<string, string>[] environment)
        {
            return runCommand(cmd, args, null, null, null, environment);
        }

        public static System.Diagnostics.Process runCommand(string cmd, string args, string curDir)
        {
            return runCommand(cmd, args, null, null, curDir);
        }

        public static System.Diagnostics.Process runCommand(string cmd, string args, string curDir, Tuple<string, string>[] environment)
        {
            return runCommand(cmd, args, null, null, curDir, environment);
        }

        public static System.Diagnostics.Process runCommand(string cmd, string args, string userName, string password_, string curDir)
        {
            return runCommand(cmd, args, userName, password_, curDir, new Tuple<string, string>[0]);
        }

        public static System.Diagnostics.Process runCommand(string cmd, string args, string userName, string password_, string curDir, Tuple<string, string>[] environment)
        {
            System.Diagnostics.Process proc = runCommandStart(cmd, args, userName, password_, curDir, environment);
            runCommandComplete(proc);
            Console.WriteLine(output_str);
            return proc;
        }

        public static System.Diagnostics.Process runCommandStart(string cmd, string args)
        {
            return runCommandStart(cmd, args, null, null, null);
        }

        public static System.Diagnostics.Process runCommandStart(string cmd, string args, Tuple<string, string>[] environment)
        {
            return runCommandStart(cmd, args, null, null, null, environment);
        }

        public static System.Diagnostics.Process runCommandStart(string cmd, string args, string curDir)
        {
            return runCommandStart(cmd, args, null, null, curDir);
        }

        public static System.Diagnostics.Process runCommandStart(string cmd, string args, string curDir, Tuple<string, string>[] environment)
        {
            return runCommandStart(cmd, args, null, null, curDir, environment);
        }

        public static System.Diagnostics.Process runCommandStart(string cmd, string args, string userName, string password_, string curDir)
        {
            return runCommandStart(cmd, args, userName, password_, curDir, new Tuple<string, string>[0]);
        }

        private static string output_str = null;

        private static void OutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            //This event handler is not called until process is finished.
            //When process is finished it gets called once for each line
            if (!String.IsNullOrEmpty(outLine.Data))
            {
                output_str += (Environment.NewLine + outLine.Data);
            }
        }


    }//CommandLineUtil

}//namespace
